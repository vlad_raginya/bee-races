package com.game.client.enums;

import java.io.Serializable;

public enum FlowersEnum implements Serializable {
    YELLOW,
    RED,
    BLUE
}

package com.game.client.logic.gameobject;

import com.game.client.enums.BeeMoveDirections;
import com.game.client.models.KeyState;
import com.game.client.models.ObjectViewSet;
import com.game.client.models.Point;
import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.Flower;
import com.game.client.models.gameobjects.HitBox;
import javafx.geometry.Bounds;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;

import java.util.Collection;

public interface IGameObjectLogic {
    HitBox createHitBox(Bounds bounds);

    HitBox createHitBox(double x, double y, double distance);

    Bee moveBee(KeyCode keyCode, Bee bee, double maxWidth, double maxHeight, KeyState keyState);

    Bee moveBee(BeeMoveDirections moveDirection, Bee bee, double maxWidth, double maxHeight);

    boolean isHitBoxesCrossed(HitBox hitBoxA, HitBox hitBoxB);

    boolean isValidKey(KeyCode keyCode);

    boolean isPointContainsFlower(Point randomPoint, Collection<ObjectViewSet<Button, Flower>> flowersView, double distance);
}

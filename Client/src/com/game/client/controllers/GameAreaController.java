package com.game.client.controllers;

import com.game.client.enums.BeeColors;
import com.game.client.enums.FlowersEnum;
import com.game.client.enums.HiveColors;
import com.game.client.logic.GameFileLogic;
import com.game.client.logic.Pauseable;
import com.game.client.logic.delegates.*;
import com.game.client.logic.gameobject.impl.GameObjectLogic;
import com.game.client.logic.gameobject.IGameObjectLogic;
import com.game.client.logic.managers.*;
import com.game.client.logic.managers.impl.*;
import com.game.client.models.*;
import com.game.client.models.factory.BeeFactory;
import com.game.client.models.factory.BeeHiveFactory;
import com.game.client.models.factory.FlowerFactory;
import com.game.client.models.factory.GameObjectCreator;
import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.BeeHive;
import com.game.client.models.gameobjects.Flower;
import com.game.client.models.gameobjects.HitBox;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import javafx.event.ActionEvent;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class GameAreaController implements Pauseable {
    private HashMap<Integer, ObjectViewSet<Button, Flower>> flowersView;
    private BeeHive enemyBeeHive, mainCharacterBeeHive;
    private Bee enemyBee, mainCharacterBee;
    private GameObjectCreator<Bee> beeFactory;
    private GameObjectCreator<Flower> flowerFactory;
    private GameObjectCreator<BeeHive> beeHiveFactory;
    private IGameObjectLogic gameObjectLogic;
    private GenerationManager flowersManager;
    private IGameCountManager countManager;
    private IEnemyManager enemyManager;
    private ITimerManager timerManager;
    private IBeeMovementManager beeMovementManager;
    private KeyState currentKeyState;
    private ReentrantLock enemyManagerLocker;
    private GameFileLogic<GameSave> fileLogic;

    private boolean isGameOnPause = false;
    private static int MAX_FLOWERS_COUNT = 5;

    @FXML
    private Button houseMain;
    @FXML
    private Button houseEnemy;
    @FXML
    private Button mainBeeView;
    @FXML
    private Button enemyBeeView;
    @FXML
    private AnchorPane areaPane;
    @FXML
    private AnchorPane popUp;
    @FXML
    private Label mainCount;
    @FXML
    private Label enemyCount;
    @FXML
    private Label timerLabel;

    public GameAreaController() {
        beeFactory = new BeeFactory();
        beeHiveFactory = new BeeHiveFactory();
        gameObjectLogic = new GameObjectLogic();
        flowersManager = new FlowersSpawnerManager();
        flowerFactory = new FlowerFactory();
        countManager = new GameCountManager();
        currentKeyState = new KeyState();
        fileLogic = new GameFileLogic(GameSave.class);

        enemyManagerLocker = new ReentrantLock();

        flowersView = new HashMap<Integer, ObjectViewSet<Button, Flower>>();
    }

    public void keyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.ESCAPE) {
            if (isGameOnPause) {
                pauseGame(false);
                return;
            }

            pauseGame(true);
        }

        if (isGameOnPause) {
            return;
        }

        if (!this.currentKeyState.activeKeys.contains(event.getCode()) && this.currentKeyState.activeKeys.size() < 2 && gameObjectLogic.isValidKey(event.getCode())) {
            this.currentKeyState.activeKeys.add(event.getCode());
        } else {
            return;
        }

        beeMovementManager.addSpeed(event.getCode(), this.currentKeyState);
        checkForFlowerDeliver();

        int indexToRemove = -1;

        for (Map.Entry<Integer, ObjectViewSet<Button, Flower>> item : flowersView.entrySet()) {
            if (gameObjectLogic.isHitBoxesCrossed(this.mainCharacterBee.getHitBox(),
                item.getValue().getGameObject().getHitBox())) {
                indexToRemove =  item.getKey();
            }
        }

        if (indexToRemove != -1) {
            if (this.mainCharacterBee.isHasFlower()) {
                return;
            }

            areaPane.getChildren().remove(flowersView.get(indexToRemove).getViewObject());
            pickFlower(flowersView.get(indexToRemove).getGameObject(), indexToRemove);

            flowersView.remove(flowersView.get(indexToRemove));
        }
    }

    public void keyReleased(KeyEvent event) {
        if (this.currentKeyState.activeKeys.contains(event.getCode())) {
            this.currentKeyState.activeKeys.remove(event.getCode());
            beeMovementManager.takeOffSpeed(this.currentKeyState);
        }
    }

    public void handleResumeButton(ActionEvent event) {
        pauseGame(false);
    }

    public void handleSaveButton(ActionEvent event) throws IOException {
        List<Flower> flowers = new ArrayList<Flower>();
        for (Map.Entry<Integer, ObjectViewSet<Button, Flower>> item : flowersView.entrySet()) {
            flowers.add(item.getValue().getGameObject());
        }

        GameSave dataToSave = new GameSave(mainCharacterBee, enemyBee, flowers,
                timerManager.getMinutes(), timerManager.getSeconds(),
                countManager.getMainCount(), countManager.getEnemyCount());

        fileLogic.saveGame(dataToSave);

        pauseGame(false);
    }

    public void handleExitButton(ActionEvent event) throws IOException {
        this.enemyManager.disableEnemyBee(true);
        this.timerManager.setGameOver();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/GameRegimeMenu.fxml"));
        AnchorPane personOverview = loader.load();
        GameRegimeController controller = loader.getController();
        Stage newStage = (Stage)houseMain.getScene().getWindow();
        newStage.setScene(new Scene(personOverview));
        newStage.show();
        controller.checkLoadingAvailability();
    }

    public void setUpControllerData(int gameDurationMinutes, int gameDurationSeconds) {
        Bounds mainBeeBounds = mainBeeView.localToScene(mainBeeView.getBoundsInLocal());
        HitBox mainBeeHitBox = gameObjectLogic.createHitBox(mainBeeBounds);

        Bounds enemyBeeBounds = enemyBeeView.localToScene(enemyBeeView.getBoundsInLocal());
        HitBox enemyBeeHitBox = gameObjectLogic.createHitBox(enemyBeeBounds);

        this.enemyBee = beeFactory.createObject(enemyBeeHitBox, BeeColors.RED.name());
        this.mainCharacterBee = beeFactory.createObject(mainBeeHitBox, BeeColors.YELLOW.name());

        loadBeeHives();

        createManagers(gameDurationMinutes, gameDurationSeconds);

        ObjectViewSet<Button, BeeHive> enemyBeeHiveObjectView = new ObjectViewSet<>(this.houseEnemy, this.enemyBeeHive);
        enemyManager = new EnemyManager(enemyManagerLocker, this.enemyBee, areaPane, enemyBeeView, this.countManager, enemyBeeHiveObjectView, flowerSpawnFunction, directionEditor);

        spawnFlowers(MAX_FLOWERS_COUNT);

        Thread enemyManagerThread = new Thread(enemyManager);
        enemyManagerThread.start();
    }

    private void createManagers(int timerMinutes, int timerSeconds) {
        countManager.setCountUpdateCallback(this.mainCharacterBee, mainCountUpdate);
        countManager.setCountUpdateCallback(this.enemyBee, enemyCountUpdate);
        timerManager = new TimerManager(updateGameTimer, gameOverFunction, timerMinutes, timerSeconds);
        beeMovementManager = new BeeMovementManager(50, beePositionUpdate, directionEditor, this.mainCharacterBee, areaPane.getPrefWidth(), areaPane.getPrefHeight());

        Thread timerManagerThread = new Thread(timerManager);
        Thread beeMovementManagerThread = new Thread(beeMovementManager);

        timerManagerThread.start();
        beeMovementManagerThread.start();
    }

    private void spawnFlowers(int flowerCount) {
        for (int i = 0; i < flowerCount; i++) {
            flowerSpawnFunction.spawnFlowerOnGameArea(flowerCount);
        }

        enemyManager.setGameAreaFlowers(this.flowersView);
    }

    FlowerSpawnerDelegate flowerSpawnFunction = (maxCountFlowers) -> {
        int flowerColor = flowersManager.getRandomValue(0, 3);

        Image image = getFlowerImage(FlowersEnum.values()[flowerColor]);

        Button flower = createViewFlower(image);

        Bounds flowerBounds = flower.localToScene(flower.getBoundsInLocal());
        HitBox flowerBox = gameObjectLogic.createHitBox(flowerBounds.getMaxX(), flowerBounds.getMaxY(), 40);

        Flower createdFlower = flowerFactory.createObject(flowerBox, FlowersEnum.values()[flowerColor].name());
        addFlowerToCollection(flower, createdFlower);
    };

    private void addFlowerToCollection(Button flowerView, Flower flower) {
        int indexToPut = 4;
        int lastIndex = indexToPut + MAX_FLOWERS_COUNT;

        while(indexToPut < lastIndex) {
            if (this.flowersView.containsKey(indexToPut)) {
                indexToPut ++;
                continue;
            }

            break;
        }

        ObjectViewSet viewValue = new ObjectViewSet<Button, Flower>(flowerView, flower);

        this.flowersView.put(indexToPut, viewValue);
        this.areaPane.getChildren().add(flowerView);
    }

    private Image getFlowerImage(FlowersEnum flowerColor) {
        Image image;

        switch (flowerColor) {
            case RED: {
                image = new Image(getClass().getResourceAsStream("../ui/styles/Images/flower_red.png"), 40, 40, false, false);
                break;
            }
            case YELLOW: {
                image = new Image(getClass().getResourceAsStream("../ui/styles/Images/flower_yellow.png"), 40, 40, false, false);
                break;
            }
            default: {
                image = new Image(getClass().getResourceAsStream("../ui/styles/Images/flower_blue.png"), 40, 40, false, false);
                break;
            }
        }

        return image;
    }

    private Button createViewFlower(Image image) {
        ImageView imageView = new ImageView(image);
        Button flower = new Button("", imageView);

        Stage newStage = (Stage) enemyBeeView.getScene().getWindow();
        Point point = flowersManager.generateRandomPoint(newStage.getWidth(), newStage.getHeight());

        while (gameObjectLogic.isPointContainsFlower(point, flowersView.values(), 40)) {
            point = flowersManager.generateRandomPoint(newStage.getWidth(), newStage.getHeight());
        }

        setFlowerStyles(flower, point);

        return flower;
    }

    private void setFlowerStyles(Button flower, Point point) {
        flower.setLayoutX(point.getX());
        flower.setLayoutY(point.getY());
        flower.setMaxHeight(40);
        flower.setMaxWidth(40);

        flower.setStyle("-fx-base: none;");
    }

    private void pickFlower(Flower flower, Integer key) {
        this.enemyManager.removeGameAreaFlower(key);
        this.mainCharacterBee.setPickedFlower(flower);
        this.mainCharacterBee.setHasFlower(true);
        spawnFlowers(1);
    }

    @Override
    public void pauseGame(boolean pause) {
        for (Map.Entry<Integer, ObjectViewSet<Button, Flower>> item : flowersView.entrySet()) {
            item.getValue().getViewObject().setVisible(!pause);
        }

        popUp.setVisible(pause);
        isGameOnPause = pause;
        enemyManager.pauseGame(pause);
        timerManager.pauseGame(pause);
    }

    private void checkForFlowerDeliver() {
        if (gameObjectLogic.isHitBoxesCrossed(this.mainCharacterBeeHive.getHitBox(), this.mainCharacterBee.getHitBox())
                && this.mainCharacterBee.isHasFlower()) {
            this.mainCharacterBee.setHasFlower(false);
            countManager.incrementCount(this.mainCharacterBee.getPickedFlower(), this.mainCharacterBee);
            countManager.updateViewCount(this.mainCharacterBee);
        }
    }

    CountUpdateDelegate mainCountUpdate = (s) -> {
        this.mainCount.setText(String.valueOf(countManager.getMainCount()));
    };

    CountUpdateDelegate enemyCountUpdate = (s) -> {
        this.enemyCount.setText(String.valueOf(countManager.getEnemyCount()));
    };

    TimerUpdateDelegate updateGameTimer = (minutes, seconds) -> {
        if (seconds >= 10) {
            this.timerLabel.setText(String.format("%d:%d", minutes, seconds));
        } else {
            this.timerLabel.setText(String.format("%d:0%d", minutes, seconds));
        }
    };

    BeePositionUpdateDelegate beePositionUpdate = (bee) -> {
        this.mainBeeView.setLayoutX(bee.getHitBox().getFirstPoint().getX());
        this.mainBeeView.setLayoutY(bee.getHitBox().getFirstPoint().getY());
    };

    GameOverDelegate gameOverFunction = () -> {
        this.enemyManager.disableEnemyBee(true);
        int mainScore = countManager.getMainCount();
        int enemyScore = countManager.getEnemyCount();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/EndGameMenu.fxml"));
        AnchorPane personOverview = loader.load();
        EndGameController controller = loader.getController();
        Stage newStage = (Stage)houseMain.getScene().getWindow();
        newStage.setScene(new Scene(personOverview));
        newStage.show();
        controller.setUpMenu(mainScore > enemyScore, mainScore, enemyScore);
    };

    BeePositionUpdateDelegate directionEditor = (bee) -> {
        int beeSkin = 1;

        if (bee.getColorRGBA().toUpperCase().equals(BeeColors.RED.name())) {
            beeSkin = 2;
        }

        Image image = getBeeDirectionImage(bee, beeSkin);

        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);

        if (beeSkin == 1) {
            this.mainBeeView.setBackground(background);
        } else {
            this.enemyBeeView.setBackground(background);
        }
    };

    private Image getBeeDirectionImage(Bee bee, int beeSkin) {
        Image image;

        switch (bee.getMoveDirection()) {
            case LEFT: {
                image = new Image(getClass().getResourceAsStream(String.format("../ui/styles/Images/bee_left_%d.png", beeSkin)), 50, 50, false, false);
                break;
            }
            case UP: {
                image = new Image(getClass().getResourceAsStream(String.format("../ui/styles/Images/bee_up_%d.png", beeSkin)), 50, 50, false, false);
                break;
            }
            case DOWN: {
                image = new Image(getClass().getResourceAsStream(String.format("../ui/styles/Images/bee_down_%d.png", beeSkin)), 50, 50, false, false);
                break;
            }
            default: {
                image = new Image(getClass().getResourceAsStream(String.format("../ui/styles/Images/bee_right_%d.png", beeSkin)), 50, 50, false, false);
                break;
            }
        }

        return image;
    }

    protected void finalize() throws Throwable {
        if (this.enemyManager != null) {
            this.enemyManager.disableEnemyBee(true);
        }
    }

    private void loadBeeHives() {
        Bounds mainBeeHive = this.houseMain.localToScene(houseMain.getBoundsInLocal());
        HitBox mainBeeHiveHitBox = this.gameObjectLogic.createHitBox(mainBeeHive);

        Bounds enemyBeeHive = this.houseEnemy.localToScene(houseEnemy.getBoundsInLocal());
        HitBox enemyBeeHiveHitBox = this.gameObjectLogic.createHitBox(enemyBeeHive);

        this.mainCharacterBeeHive = beeHiveFactory.createObject(mainBeeHiveHitBox, HiveColors.YELLOW.name());
        this.enemyBeeHive = beeHiveFactory.createObject(enemyBeeHiveHitBox, HiveColors.BLACK.name());
    }

    private void loadBees(GameSave loadedData) {
        this.enemyBee = loadedData.getEnemyBee();
        this.mainCharacterBee = loadedData.getMainCharacterBee();

        this.enemyBeeView.setLayoutX(this.enemyBee.getHitBox().getFirstPoint().getX());
        this.enemyBeeView.setLayoutY(this.enemyBee.getHitBox().getFirstPoint().getY());
        this.mainBeeView.setLayoutX(this.mainCharacterBee.getHitBox().getFirstPoint().getX());
        this.mainBeeView.setLayoutY(this.mainCharacterBee.getHitBox().getFirstPoint().getY());
    }

    public void loadControllerData(GameSave loadedData) {
        loadBees(loadedData);
        loadBeeHives();

        this.countManager.setEnemyCount(loadedData.getEnemyScore());
        this.countManager.setMainCount(loadedData.getMainScore());

        createManagers(loadedData.getTimeMinutes(), loadedData.getTimeSeconds());
        ObjectViewSet<Button, BeeHive> enemyBeeHiveObjectView = new ObjectViewSet<>(this.houseEnemy, this.enemyBeeHive);
        enemyManager = new EnemyManager(enemyManagerLocker, this.enemyBee, areaPane, enemyBeeView, this.countManager, enemyBeeHiveObjectView, flowerSpawnFunction, directionEditor);

        for (Flower item : loadedData.getFlowerList()) {
            Image image = getFlowerImage(item.getColor());

            ImageView imageView = new ImageView(image);
            Button flower = new Button("", imageView);

            setFlowerStyles(flower, item.getHitBox().getFirstPoint());

            addFlowerToCollection(flower, item);
        }

        enemyManager.setGameAreaFlowers(this.flowersView);

        Thread enemyManagerThread = new Thread(enemyManager);
        enemyManagerThread.start();
        countManager.updateViewCount(this.mainCharacterBee);
        countManager.updateViewCount(this.enemyBee);
    }
}

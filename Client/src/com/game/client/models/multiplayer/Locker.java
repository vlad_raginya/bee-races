package com.game.client.models.multiplayer;

public class Locker {
    boolean isLocked;

    public void lock() {
        this.isLocked = true;
    }

    public void unlock() {
        this.isLocked = false;
    }

    public boolean isLocked() {
        return this.isLocked;
    }
}

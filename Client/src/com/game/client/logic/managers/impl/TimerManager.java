package com.game.client.logic.managers.impl;

import com.game.client.logic.delegates.GameOverDelegate;
import com.game.client.logic.delegates.TimerUpdateDelegate;
import com.game.client.logic.managers.ITimerManager;
import javafx.application.Platform;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

public class TimerManager implements ITimerManager {
    private ReentrantLock locker;
    private TimerUpdateDelegate timerUpdateFunction;
    private GameOverDelegate gameOverFunction;
    private TimerTask timerTask;
    private Timer timer;

    private int minutes;
    private int seconds;
    private boolean isGameOver = false;

    private boolean isGameOnPause = false;

    @Override
    public void setGameOver() {
        this.isGameOver = true;
    }

    @Override
    public int getMinutes() {
        return minutes;
    }

    @Override
    public int getSeconds() {
        return seconds;
    }

    public TimerManager(TimerUpdateDelegate timerUpdateFunction, GameOverDelegate gameOverFunction, int minutes, int seconds) {
        this.locker = new ReentrantLock();
        this.timerUpdateFunction = timerUpdateFunction;
        this.gameOverFunction = gameOverFunction;
        this.minutes = minutes;
        this.seconds = seconds;
        timerTask = new TimerManagerTask();
        timer = new Timer(true);
    }

    public TimerManager(int minutes, int seconds) {
        this.locker = new ReentrantLock();
        this.minutes = minutes;
        this.seconds = seconds;
        timerTask = new TimerManagerTask();
        timer = new Timer(true);
    }

    @Override
    public void run() {
        timer.scheduleAtFixedRate(timerTask, 0, 1000);

        while (!isGameOver) {
            continue;
        }

        timer.cancel();
    }

    @Override
    public void pauseGame(boolean pause) {
        if (pause) {
            timer.cancel();
        } else {
            timer = new Timer(true);
            timerTask = new TimerManagerTask();
            timer.scheduleAtFixedRate(timerTask, 0, 1000);
        }
    }

    private class TimerManagerTask extends TimerTask {
        @Override
        public void run() {
            if (!isGameOver) {
                locker.lock();
                if (minutes == 0 && seconds == 0) {
                    isGameOver = true;

                    Platform.runLater(() -> {
                        try {
                            gameOverFunction.finishTheGame();
                        } catch (Exception e) {
                            timer.cancel();
                            return;
                        }
                    });
                } else if (minutes != 0 && seconds == 0) {
                    minutes--;
                    seconds = 60;
                } else {
                    seconds--;
                }

                locker.unlock();

                Platform.runLater(() -> {
                    timerUpdateFunction.updateGameTimer(minutes, seconds);
                });
            }
        }
    }
}

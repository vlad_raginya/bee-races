package com.game.shared.models;

public abstract class Packet {
    public enum PacketTypes {
        INVALID(-1),
        GAMESTATE(00),
        CONNECT(01),
        DISCONNECT(11);

        private int packetId;

        private PacketTypes(int packetId) {
            this.packetId = packetId;
        }

        public int getPacketId() {
            return this.packetId;
        }
    }

    private byte packetId;

    public Packet(int packetId) {
        this.packetId = (byte)packetId;
    }

    public byte getPacketId() {
        return this.packetId;
    }

    public static PacketTypes lookupPaket(int id) {
        for (PacketTypes packetType : PacketTypes.values()) {
            if (packetType.getPacketId() == id) {
                return packetType;
            }
        }

        return PacketTypes.INVALID;
    }

    public static PacketTypes lookupPaket(String id) {
        try {
            return lookupPaket(Integer.parseInt(id));
        } catch (NumberFormatException ex) {
            return PacketTypes.INVALID;
        }
    }

    public abstract void writeData();

    public abstract byte[] getData();

    public String readData(byte[] data) {
        String message = new String(data).trim();
        return message.substring(2);
    }
}

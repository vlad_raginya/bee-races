package com.game.client.enums;

import java.io.Serializable;

public enum BeeMoveDirections implements Serializable {
    RIGHT,
    LEFT,
    UP,
    DOWN,
    RIGHTUP,
    RIGHTDOWN,
    LEFTUP,
    LEFTDOWN
}

package com.game.client.models.gameobjects;

import com.game.client.models.gameobjects.base.GameObject;

public class BeeHive extends GameObject {
    private boolean isContainsBee;
    private int honeyContained;

    public BeeHive(String color, HitBox hitBox) {
        this.isContainsBee = false;
        this.honeyContained = 0;
        this.colorRGBA = color;
        this.hitBox = hitBox;
    }

    public int getHoneyContained() {
        return this.honeyContained;
    }

    public boolean isContainsBee() {
        return this.isContainsBee;
    }

    public void setContainsBee(boolean containsBee) {
        this.isContainsBee = containsBee;
    }

    public void setHoneyContained(int honeyContained) {
        this.honeyContained = honeyContained;
    }
}

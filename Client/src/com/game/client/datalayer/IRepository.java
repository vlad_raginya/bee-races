package com.game.client.datalayer;

import com.game.client.models.GameSave;

import java.io.IOException;

public interface IRepository<T> {
    public void saveFile(T data, String pathname) throws IOException;
    public T loadFile(String pathname);
    public boolean isFileExist(String pathname);
}

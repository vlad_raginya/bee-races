package com.game.client.logic.managers;

import com.game.client.logic.Pauseable;
import com.game.client.models.KeyState;
import javafx.scene.input.KeyCode;

public interface IBeeMovementManager extends Runnable, Pauseable {
    void addSpeed(KeyCode keyCode, KeyState keyState);

    void takeOffSpeed(KeyState keyState) ;
}

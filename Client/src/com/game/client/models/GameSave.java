package com.game.client.models;

import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.Flower;
import javafx.scene.control.Button;

import java.util.HashMap;
import java.util.List;

public class GameSave {
    private Bee mainCharacterBee;
    private Bee enemyBee;
    private int timeMinutes;
    private int timeSeconds;
    private int mainScore;
    private int enemyScore;
    private List<Flower> flowerList;

    public GameSave() {}

    public GameSave(Bee main, Bee enemy, List<Flower> flowerForBee,
                    int minutes, int seconds, int mainCount, int enemyCount) {
        this.mainCharacterBee = main;
        this.enemyBee = enemy;
        this.flowerList = flowerForBee;
        this.timeMinutes = minutes;
        this.timeSeconds = seconds;
        this.mainScore = mainCount;
        this.enemyScore = enemyCount;
    }

    public Bee getEnemyBee() {
        return enemyBee;
    }

    public Bee getMainCharacterBee() {
        return mainCharacterBee;
    }

    public int getEnemyScore() {
        return enemyScore;
    }

    public int getMainScore() {
        return mainScore;
    }

    public int getTimeMinutes() {
        return timeMinutes;
    }

    public int getTimeSeconds() {
        return timeSeconds;
    }

    public List<Flower> getFlowerList() {
        return flowerList;
    }

    public void setEnemyBee(Bee enemyBee) {
        this.enemyBee = enemyBee;
    }

    public void setMainCharacterBee(Bee mainCharacterBee) {
        this.mainCharacterBee = mainCharacterBee;
    }

    public void setEnemyScore(int enemyScore) {
        this.enemyScore = enemyScore;
    }

    public void setFlowerList(List<Flower> flowerList) {
        this.flowerList = flowerList;
    }

    public void setMainScore(int mainScore) {
        this.mainScore = mainScore;
    }

    public void setTimeMinutes(int timeMinutes) {
        this.timeMinutes = timeMinutes;
    }

    public void setTimeSeconds(int timeSeconds) {
        this.timeSeconds = timeSeconds;
    }
}

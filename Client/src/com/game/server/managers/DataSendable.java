package com.game.server.managers;

import com.game.client.models.multiplayer.GameState;

import java.io.IOException;

public interface DataSendable extends Runnable {
    GameState readData() throws IOException;

    void sendData(GameState gameState) throws IOException;
}

package com.game.shared.models;

public class DisconnectPacket extends Packet {
    private String uerId;
    private String username;

    public DisconnectPacket(byte[] data) {
        super(11);
        this.uerId = readData(data);
    }

    public String getUsername() {
        return this.username;
    }

    public String getUserId () {
        return this.uerId;
    }

    @Override
    public byte[] getData() {
        return ("11" + this.username).getBytes();
    }

    @Override
    public void writeData() {

    }
}

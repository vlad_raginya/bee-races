package com.game.client.models.factory;

import com.game.client.enums.HiveColors;
import com.game.client.models.Point;
import com.game.client.models.gameobjects.BeeHive;
import com.game.client.models.gameobjects.HitBox;

public class BeeHiveFactory implements GameObjectCreator<BeeHive> {
    @Override
    public BeeHive createObject(double firstX, double firstY, double secondX, double secondY, String color) {
        HitBox hiveBox = new HitBox(new Point(firstX, firstY), new Point(secondX, secondY));

        if (hiveBox != null) {
            return createBeeHive(hiveBox, color);
        }

        return new BeeHive("BLACK", new HitBox(new Point(0.0, 0.0), new Point(0.0, 0.0)));
    }

    @Override
    public BeeHive createObject(HitBox hitBox, String color) {
        if (hitBox != null) {
            return createBeeHive(hitBox, color);
        }

        return new BeeHive("YELLOW", new HitBox(new Point(0.0, 0.0), new Point(0.0, 0.0)));
    }

    private BeeHive createBeeHive(HitBox hiveBox, String color) {
        HiveColors hiveColor = HiveColors.valueOf(color.toUpperCase());

        if (hiveColor != null) {
            if (hiveColor.name().toUpperCase().equals("YELLOW")
                    || hiveColor.name().toUpperCase().equals("BLACK")) {
                return new BeeHive(color, hiveBox);
            }
        }

        return new BeeHive(HiveColors.YELLOW.toString(), hiveBox);
    }
}

package com.game.client.models.factory;

import com.game.client.models.gameobjects.HitBox;
import com.game.client.models.gameobjects.base.GameObject;

public interface GameObjectCreator<T> {
    T createObject(double firstX, double firstY, double secondX, double secondY, String color);

    T createObject(HitBox hitBox, String color);
}

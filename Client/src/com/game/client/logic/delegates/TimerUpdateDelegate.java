package com.game.client.logic.delegates;

public interface TimerUpdateDelegate {
    void updateGameTimer(int minutes, int seconds);
}

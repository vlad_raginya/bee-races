package com.game.client.logic.delegates;

import com.game.client.models.multiplayer.GameState;
import com.game.client.models.multiplayer.Locker;

public interface GameUpdateDelegate {
    void updateMPGameArea(GameState gameState, Locker locker);
}

package com.game.client.logic.delegates;

import java.io.IOException;

public interface GameOverDelegate {
    void finishTheGame() throws IOException;
}

package com.game.client.models.gameobjects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.game.client.enums.BeeMoveDirections;
import com.game.client.enums.FlowersEnum;
import com.game.client.models.gameobjects.base.GameObject;

import java.io.Serializable;


public class Bee extends GameObject implements Serializable {

    private Flower pickedFlower;

    private double speed;

    private boolean hasFlower;

    private BeeMoveDirections moveDirection;

    @JsonIgnore
    private boolean isDirectionSkinEdit;

    public Bee() {
        this.hasFlower = false;
        this.moveDirection = BeeMoveDirections.RIGHT;
    }

    public Bee(String color, HitBox hitBox, double speed) {
        this.pickedFlower = null;
        this.speed = speed;
        this.colorRGBA = color;
        this.hitBox = hitBox;
        this.hasFlower = false;
        this.moveDirection = BeeMoveDirections.RIGHT;
    }

    public Bee(String color, HitBox hitbox) {
        super(color, hitbox);
        this.speed = 5;
        this.hasFlower = false;
        this.moveDirection = BeeMoveDirections.RIGHT;
    }

    public Bee updateHitBox(HitBox newHitBox) {
        if (newHitBox != null) {
            this.hitBox = newHitBox;
        }

        return this;
    }

    public double getSpeed() {
        return speed;
    }

    public Flower getPickedFlower() {
        return pickedFlower;
    }

    public void setPickedFlower(Flower pickedFlower) {
        this.pickedFlower = pickedFlower;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public boolean isHasFlower() {
        return this.hasFlower;
    }

    public void setHasFlower(boolean hasFlower) {
        this.hasFlower = hasFlower;
    }

    public void setMoveDirection(BeeMoveDirections direction) {
        this.moveDirection = direction;
    }

    public BeeMoveDirections getMoveDirection() {
        return this.moveDirection;
    }

    @JsonIgnore
    public boolean isDirectionSkinEdit() {
        return isDirectionSkinEdit;
    }

    @JsonIgnore
    public void setDirectionSkinEdit(boolean directionSkinEdit) {
        this.isDirectionSkinEdit = directionSkinEdit;
    }

    @JsonIgnore
    public boolean isFull() {
        return pickedFlower != null;
    }
}

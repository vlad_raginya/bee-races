package com.game.client.models.gameobjects;

import com.game.client.enums.FlowersEnum;
import com.game.client.models.gameobjects.base.GameObject;

import java.io.Serializable;

public class Flower extends GameObject implements Serializable {
    private FlowersEnum color;
    private int lifeTime;

    public Flower(String colorRGBA, HitBox hitBox, FlowersEnum color, int lifeTime ) {
        this.color = color;
        this.lifeTime = lifeTime;
        this.colorRGBA = colorRGBA;
        this.hitBox = hitBox;
    }

    public Flower() {}

    public FlowersEnum getColor() {
        return this.color;
    }

    public int getLifeTime() {
        return this.lifeTime;
    }

    public void setColor(FlowersEnum color) {
        this.color = color;
    }

    public void setLifeTime(int lifeTime) {
        this.lifeTime = lifeTime;
    }
}

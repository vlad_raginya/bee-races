package com.game.server.logic.impl;

import com.game.client.models.GameSave;
import com.game.server.logic.IServerLogic;

import java.io.*;

public class ServerLogic<T> implements IServerLogic<T> {
    @Override
    public T deserialize(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        T obj = (T) is.readObject();
        in.close();
        is.close();
        return obj;
    }

    @Override
    public byte[] serialize(T data) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(data);
        byte[] byteArr = out.toByteArray();
        out.close();
        os.close();
        return byteArr;
    }
}

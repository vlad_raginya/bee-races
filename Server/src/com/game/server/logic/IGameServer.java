package com.game.server.logic;

import java.net.InetAddress;

public interface IGameServer {
    void start();
    void sendData(byte[] data, InetAddress ipAddress, int port);
    void sendToAllClients(byte[] data);
    void parsePacket(byte[] data, InetAddress ipAddress, int port);
}

package com.game.client.models;

import java.io.Serializable;

public class ObjectViewSet<T, Y> implements Serializable {
    public ObjectViewSet(T viewObject, Y gameObject) {
        this.viewObject = viewObject;
        this.gameObject = gameObject;
    }

    public ObjectViewSet() {

    }

    private T viewObject;

    private Y gameObject;

    public Y getGameObject() {
        return gameObject;
    }

    public T getViewObject() {
        return viewObject;
    }
}

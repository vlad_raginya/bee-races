package com.game.client.models.multiplayer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class ConnectionResult extends MpResult {
    private Socket socket;

    private DataOutputStream outputStream;

    private DataInputStream inputStream;

    public ConnectionResult(Socket socket, DataInputStream inputStream, DataOutputStream outputStream) {
        this.socket = socket;
        this.isSuccessfully = socket != null;
        this.message = "";
        this.outputStream = outputStream;
        this.inputStream = inputStream;
    }

    public ConnectionResult(String message) {
        this.socket = null;
        this.isSuccessfully = false;
        this.message = message;
    }

    public boolean isSuccessfully() {
        return this.isSuccessfully;
    }

    public Socket getSocket() {
        return this.socket;
    }

    public String getMessage() {
        return this.message;
    }

    public DataInputStream getInputStream() {
        return this.inputStream;
    }

    public DataOutputStream getOutputStream() {
        return this.outputStream;
    }
}

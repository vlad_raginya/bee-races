package com.game.client.models;

import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.List;

public class KeyState {
    public KeyState() {
        this.activeKeys = new ArrayList<KeyCode>();
    }

    public boolean isWPressed;

    public boolean isSPressed;

    public boolean isAPressed;

    public boolean isDPressed;

    public List<KeyCode> activeKeys;
}

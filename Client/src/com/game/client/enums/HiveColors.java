package com.game.client.enums;

import java.io.Serializable;

public enum HiveColors implements Serializable {
    YELLOW,
    BLACK
}

package com.game.client.models.multiplayer;

import java.net.ServerSocket;

public class ServerState extends MpResult {
    private ServerSocket serverSocket;

    public ServerState(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
        this.isSuccessfully = serverSocket != null;
        this.message = "";
    }

    public ServerState(String message) {
        this.serverSocket = null;
        this.isSuccessfully = false;
        this.message = message;
    }

    public boolean isSuccessfully() {
        return this.isSuccessfully;
    }

    public ServerSocket getServerSocket() {
        return this.serverSocket;
    }

    public String getMessage() {
        return this.message;
    }
}

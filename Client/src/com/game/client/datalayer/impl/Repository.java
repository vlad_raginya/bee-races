package com.game.client.datalayer.impl;

import com.game.client.datalayer.IRepository;
import com.game.client.datalayer.JSONParser;
import com.game.client.models.GameSave;
import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.Flower;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Repository<T> implements IRepository<T> {

    final Class<T> typeOfT;

    public Repository(Class<T> typeOfT) {
        this.typeOfT = typeOfT;
    }

    @Override
    public void saveFile(T data, String pathname) throws IOException {
        JSONParser parser = new JSONParser();
        File file = new File(pathname);
        if(!file.exists()) file.createNewFile();
        parser.saveObject(file, data);
    }

    @Override
    public T loadFile(String pathname) {
        JSONParser parser = new JSONParser();
        T data = null;
        File file = new File(pathname);
        data = (T)parser.getObject(file, typeOfT);

        return data;
    }

    @Override
    public boolean isFileExist(String pathname) {
        File file = new File(pathname);
        return file.exists();
    }
}

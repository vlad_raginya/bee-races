package com.game.server.managers.impl;

import com.game.client.logic.delegates.GameAreaViewerDelegate;
import com.game.client.logic.delegates.GameOverDelegate;
import com.game.client.logic.delegates.GameUpdateDelegate;
import com.game.client.logic.delegates.TimerUpdateDelegate;
import com.game.client.logic.managers.ITimerManager;
import com.game.client.logic.managers.impl.TimerManager;
import com.game.client.models.multiplayer.ConnectionResult;
import com.game.client.models.multiplayer.GameState;
import com.game.client.models.multiplayer.Locker;
import com.game.client.models.multiplayer.ServerState;
import com.game.server.logic.IServerLogic;
import com.game.server.logic.impl.ServerLogic;
import com.game.server.managers.DataSendable;
import com.game.server.managers.IServerManager;
import javafx.application.Platform;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.ReentrantLock;

public class ServerManager implements IServerManager {
    private IServerLogic<GameState> serverLogic;
    private ITimerManager timerManager;
    private ServerSocket serverSocket;
    private Thread serverThread;
    private Thread listenerThread;

    private DataOutputStream serverDataOutputStream;
    private DataInputStream serverDataInputStream;

    private DataOutputStream listenerDataOutputStream;
    private DataInputStream listenerDataInputStream;
    private Socket listenerSocket;

    private boolean isServerRunning = false;
    private boolean isUserJoined = false;
    private boolean isConnectedSuccessfully = false;
    private boolean isListenerRunning = false;

    private ReentrantLock locker;
    private Locker customLocker;

    private GameUpdateDelegate gameUpdateCallback;
    private GameAreaViewerDelegate gameAreaViewerCallback;

    private int serverMinutes;
    private int serverSeconds;

    private int durationMinutes;
    private int durationSeconds;

    public ServerManager(GameUpdateDelegate gameUpdateCallback,
                         GameAreaViewerDelegate gameAreaViewerCallback,
                         int durationMinutes,
                         int durationSeconds) {
        this.serverLogic = new ServerLogic();
        this.gameUpdateCallback = gameUpdateCallback;
        this.gameAreaViewerCallback = gameAreaViewerCallback;
        this.durationMinutes = durationMinutes;
        this.durationSeconds = durationSeconds;
        this.locker = new ReentrantLock();
        this.customLocker = new Locker();
    }

    public ServerManager(int durationMinutes, int durationSeconds) {
        this.serverLogic = new ServerLogic();
        this.durationMinutes = durationMinutes;
        this.durationSeconds = durationSeconds;
        this.locker = new ReentrantLock();
        this.customLocker = new Locker();
    }

    @Override
    public void joinToServer(boolean join) {
        if (isConnectedSuccessfully) {
            if (join) {
                listenerThread = new Thread(new ServerListener());
                isListenerRunning = true;
                listenerThread.start();
                isUserJoined = true;
            } else {
                isListenerRunning = false;
            }
        }
    }

    @Override
    public void setGameAreaViewerCallback(GameAreaViewerDelegate gameAreaViewerCallback) {
        if (gameAreaViewerCallback != null) {
            this.gameAreaViewerCallback = gameAreaViewerCallback;
        }
    }

    @Override
    public void setGameUpdateCallback(GameUpdateDelegate gameUpdateCallback) {
        if (gameUpdateCallback != null) {
            this.gameUpdateCallback = gameUpdateCallback;
        }
    }

    @Override
    public ConnectionResult connectToServer(String ip, int port) {
        try {
            this.listenerSocket = new Socket(ip, port);
            this.listenerDataOutputStream = new DataOutputStream(this.listenerSocket.getOutputStream());
            this.listenerDataInputStream = new DataInputStream(this.listenerSocket.getInputStream());
            this.isConnectedSuccessfully = true;
            return new ConnectionResult(this.listenerSocket, this.listenerDataInputStream, this.listenerDataOutputStream);
        } catch (Exception ex) {
            return new ConnectionResult("Unable to connect tot the: " + ip + ":" + port);
        }
    }

    @Override
    public ServerState hostGame(String ip, int port) {
        try {
            serverSocket = new ServerSocket(port, 8, InetAddress.getByName(ip));

            return new ServerState(serverSocket);
        } catch (Exception ex) {
            return new ServerState("Cannot host the game");
        }
    }

    @Override
    public void startServer(boolean start) {
        if (start && !isServerRunning) {
            isServerRunning = true;

            serverThread = new Thread(new Server());
            serverThread.start();
        } else {
            if (isServerRunning) {
                isServerRunning = false;
            }
        }
    }

    @Override
    public boolean isUserJoined() {
        return this.isUserJoined;
    }

    private class Server implements DataSendable {
        @Override
        public void run() {
            if (!isUserJoined && isServerRunning) {
                listenForJoinRequest();
            }

            timerManager = new TimerManager(serverTimerUpdateCallback, gameOverCallback, durationMinutes, durationSeconds);
            Thread timerThread = new Thread(timerManager);
            if (isServerRunning && isUserJoined) {
                timerThread.start();
            }

            while (isServerRunning && isUserJoined) {
                try {
                    locker.lock();
                    GameState newHostGameState = gameAreaViewerCallback.getGameAreaState();
                    locker.unlock();
                    newHostGameState.setTimeMinutes(serverMinutes);
                    newHostGameState.setTimeSeconds(serverSeconds);

                    if (newHostGameState != null) {
                        sendData(newHostGameState);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                try {
                    GameState readingData = null;
                    if ((readingData = readData()) != null) {
                        readingData.setTimeSeconds(serverSeconds);
                        readingData.setTimeMinutes(serverMinutes);

                        gameUpdateCallback.updateMPGameArea(readingData, customLocker);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        @Override
        public GameState readData() throws IOException {
            byte[] buffer = new byte[102400];
            GameState portionOfData = null;

            serverDataInputStream.read(buffer);

            try {
                portionOfData = serverLogic.deserialize(buffer);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return portionOfData;
        }

        @Override
        public void sendData(GameState newHostGameState) throws IOException {
            byte[] buffer = serverLogic.serialize(newHostGameState);

            serverDataOutputStream.write(buffer);
            serverDataOutputStream.flush();
        }

        private void listenForJoinRequest() {
            Socket socket;

            try {
                socket = serverSocket.accept();
                serverDataInputStream = new DataInputStream(socket.getInputStream());
                serverDataOutputStream = new DataOutputStream(socket.getOutputStream());

                isUserJoined = true;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        TimerUpdateDelegate serverTimerUpdateCallback = (minutes, seconds) -> {
            serverMinutes = minutes;
            serverSeconds = seconds;
        };

        GameOverDelegate gameOverCallback = () -> {

        };
    }

    public class ServerListener implements DataSendable {
        @Override
        public void run() {
            while (isListenerRunning) {
                try {
                    GameState readingData = null;
                    if ((readingData = readData()) != null) {
                        customLocker.lock();
                        GameState finalReadingData = readingData;
                        gameUpdateCallback.updateMPGameArea(finalReadingData, customLocker);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                try {
                    while (customLocker.isLocked()) {}
                    customLocker.lock();
                    GameState newGameState = gameAreaViewerCallback.getGameAreaState();
                    customLocker.unlock();

                    if (newGameState != null) {
                        sendData(newGameState);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        @Override
        public GameState readData() throws IOException {
            byte[] buffer = new byte[102400];
            GameState portionOfData = null;

            listenerDataInputStream.read(buffer);

            try {
                portionOfData = serverLogic.deserialize(buffer);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return portionOfData;
        }

        @Override
        public void sendData(GameState gameState) throws IOException {
            byte[] buffer = serverLogic.serialize(gameState);

            listenerDataOutputStream.write(buffer);
            listenerDataOutputStream.flush();
        }
    }
}

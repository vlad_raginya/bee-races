package com.game.server.logic;

import com.game.shared.models.Player;
import com.game.shared.models.Packet;
import com.game.shared.models.ConnectPacket;
import com.game.shared.models.DisconnectPacket;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class GameServer implements IGameServer, Runnable {
    private DatagramSocket socket;
    private List<Player> connectedPlayers = new ArrayList<>();

    private int port;
    private boolean isRunning = false;

    public GameServer(int port, int bufferSize) {
        try {
            this.socket = new DatagramSocket(port);
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
    }

    public void start() {
        new Thread(this).start();
    }

    @Override
    public void run() {
        while(isRunning) {
            byte[] receivedData = new byte[1024];
            DatagramPacket receivedPacket = new DatagramPacket(receivedData, receivedData.length);

            try {
                socket.receive(receivedPacket);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            parsePacket(receivedPacket.getData(), receivedPacket.getAddress(), receivedPacket.getPort());
        }
    }

    public void parsePacket(byte[] data, InetAddress ipAddress, int port) {
        String message = new String(data).trim();
        Packet.PacketTypes packetType = Packet.lookupPaket(message.substring(0, 2));

        switch (packetType) {
            case INVALID:
                break;
            case GAMESTATE:
                break;
            case CONNECT:
                connectToServer(data, ipAddress, port);
                break;
            case DISCONNECT:
                disconnectFromServer(data, ipAddress, port);
                break;
            default:
        }
    }

    public void sendData(byte[] data, InetAddress ipAddress, int port) {
        DatagramPacket sendPacket = new DatagramPacket(data, data.length, ipAddress, port);

        try {
            this.socket.send(sendPacket);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void sendToAllClients(byte[] data) {
        for (Player player : connectedPlayers) {
            sendData(data, player.getIpAddress(), player.getPort());
        }
    }

    private void connectToServer(byte[] data, InetAddress ipAddress, int port) {
        ConnectPacket connectPacket = new ConnectPacket(data);
        System.out.println("[" + ipAddress.getHostAddress() + ":" + port + "] " + connectPacket.getUsername() + "has connected...");
        Player player = null;
        if (ipAddress.getHostAddress().equalsIgnoreCase("127.0.0.1")) {
            player = new Player(port, ipAddress);
        }
        else  {
            // Will be create by other way
            player = new Player(port, ipAddress);
        }

        if (player != null) {
            this.connectedPlayers.add(player);
        }
    }

    private void disconnectFromServer(byte[] data, InetAddress ipAddress, int port) {
        DisconnectPacket disconnectPacket = new DisconnectPacket(data);
        System.out.println("[" + ipAddress.getHostAddress() + ":" + port + "] " + disconnectPacket.getUsername() + "has disconnected...");
        Player disconnectedPlayer = connectedPlayers.stream()
                .filter(x -> x.getId() == disconnectPacket.getUserId())
                .collect(Collectors.toList()).get(0);

        if (disconnectedPlayer != null) {
            connectedPlayers.remove(disconnectedPlayer);
        }
    }
}
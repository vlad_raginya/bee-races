package com.game.client.models;

public class PlayerData {

    private String name;
    private Integer score;

    public PlayerData() { }

    public PlayerData(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public Integer getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(int score) {
        this.score = score;
    }
}

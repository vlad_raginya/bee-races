package com.game.client.logic.delegates;

import com.game.client.models.multiplayer.GameState;

// Called each tick and send game state to the server
public interface GameAreaViewerDelegate {
    GameState getGameAreaState();
}

package com.game.client.logic.delegates;

public interface FlowerSpawnerDelegate {
    void spawnFlowerOnGameArea(int maxCountFlower);
}

package com.game.client.controllers;

import com.game.client.models.KeyState;
import com.game.client.models.multiplayer.ConnectionResult;
import com.game.client.models.multiplayer.ServerState;
import com.game.server.logic.IServerLogic;
import com.game.server.logic.impl.ServerLogic;
import com.game.server.managers.IServerManager;
import com.game.server.managers.impl.ServerManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MultiplayerMenuController {
    private IServerLogic serverLogic;
    private IServerManager serverManager;

    @FXML
    private TextField ipField;
    @FXML
    private TextField portField;

    public MultiplayerMenuController() {
        this.serverLogic = new ServerLogic();
        this.serverManager = new ServerManager(1, 30);
    }

    public void handleJoinGameClick(ActionEvent event) throws Exception {
        String ip;
        int port;

        ip = getIp();
        port = getPort();

        ConnectionResult joinResult = serverManager.connectToServer(ip, port);

        if (joinResult.isSuccessfully()) {
            startMultiplayerGame(false, serverManager);
        } else {

        }
    }

    public void handleHostGameClick(ActionEvent event) throws Exception {
        String ip;
        int port;

        ip = getIp();
        port = getPort();

        ServerState hostState = serverManager.hostGame(ip, port);

        if (hostState.isSuccessfully()) {
            startMultiplayerGame(true, serverManager);
        } else {

        }
    }

    public void handleBackButtonClick(ActionEvent event) throws Exception {
        Stage newStage = (Stage)ipField.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/GameRegimeMenu.fxml"));
        Parent root = loader.load();
        GameRegimeController controller = loader.getController();
        newStage.setScene(new Scene(root));
        newStage.show();
        controller.checkLoadingAvailability();
    }

    private String getIp() {
        String ip;

        if (!ipField.getText().isEmpty()) {
            ip = ipField.getText();
        } else {
            // throw exception
            ip = "localhost";
        }

        return ip;
    }

    private int getPort() {
        int port;

        if (!portField.getText().isEmpty()) {
            port = Integer.parseInt(portField.getText());
        } else {
            // throw exception
            return 0;
        }
        List<KeyState> test = new ArrayList<>();

        return port;
    }

    private void startMultiplayerGame(boolean isGameHost, IServerManager serverManager) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/MPGameArea.fxml"));
        AnchorPane mpGameArea = loader.load();
        MultiplayerGameController controller = loader.getController();
        Stage newStage = (Stage)ipField.getScene().getWindow();
        newStage.setScene(new Scene(mpGameArea));
        newStage.show();
        controller.setUpControllerData(isGameHost, serverManager);
    }
}

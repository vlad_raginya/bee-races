package com.game.client.controllers;

import com.game.client.logic.GameFileLogic;
import com.game.client.models.GameSave;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class GameRegimeController {

    @FXML
    private Button back;
    @FXML
    private Button load;

    private GameFileLogic<GameSave> loadingLogic;

    public GameRegimeController() {
        loadingLogic = new GameFileLogic(GameSave.class);
    }


    public void handleSingleplayerButton(ActionEvent event) throws Exception {
        setGameStartArea();
    }

    public void handleBackButton(ActionEvent event) throws Exception {
        Stage newStage = (Stage) back.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("../ui/Menu.fxml"));
        newStage.setScene(new Scene(root));
        newStage.show();
    }

    public void handleLoadButton(ActionEvent event) throws Exception {

        GameSave loadedData = loadingLogic.loadGame();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/GameArea.fxml"));
        AnchorPane personOverview = loader.load();
        GameAreaController controller = loader.getController();
        Stage newStage = (Stage)back.getScene().getWindow();
        newStage.setScene(new Scene(personOverview));
        newStage.show();
        controller.loadControllerData(loadedData);
    }

    public void handleMultiplayerButton(ActionEvent event) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/MultiplayerMenu.fxml"));
        AnchorPane personOverview = loader.load();
        Stage newStage = (Stage)back.getScene().getWindow();
        newStage.setScene(new Scene(personOverview));
        newStage.show();
    }

    private void setGameStartArea() throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/GameArea.fxml"));
        AnchorPane personOverview = loader.load();
        GameAreaController controller = loader.getController();
        Stage newStage = (Stage)back.getScene().getWindow();
        newStage.setScene(new Scene(personOverview));
        newStage.show();
        controller.setUpControllerData(1, 30);
    }

    public void checkLoadingAvailability() {
        load.setDisable(!loadingLogic.isSaveFileExist());
    }
}

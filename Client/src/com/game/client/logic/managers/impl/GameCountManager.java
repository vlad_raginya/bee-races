package com.game.client.logic.managers.impl;

import com.game.client.enums.BeeColors;
import com.game.client.logic.delegates.CountUpdateDelegate;
import com.game.client.logic.managers.IGameCountManager;
import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.Flower;

public class GameCountManager implements IGameCountManager {
    private int mainCharacterCount;

    private int enemyCharacterCount;
    private CountUpdateDelegate mainCountUpdateDelegate;
    private CountUpdateDelegate enemyCountUpdateDelegate;

    @Override
    public int incrementCount(Flower flower, Bee beeToIncrement) {
        if (beeToIncrement.getColorRGBA().toUpperCase().equals(BeeColors.YELLOW.name())) {
            this.mainCharacterCount += (flower.getColor().ordinal() + 1);
        }
        else {
            this.enemyCharacterCount += (flower.getColor().ordinal() + 1);
        }

        return flower.getColor().ordinal() + 1;
    }

    @Override
    public int getEnemyCount() {
        return this.enemyCharacterCount;
    }

    @Override
    public int getMainCount() {
        return this.mainCharacterCount;
    }

    @Override
    public void setEnemyCount(int count) { this.enemyCharacterCount = count; }

    @Override
    public void setMainCount(int count) { this.mainCharacterCount = count; }

    @Override
    public void setCountUpdateCallback(Bee bee, CountUpdateDelegate callback) {
        if (bee.getColorRGBA().toUpperCase().equals(BeeColors.RED.name())) {
            this.enemyCountUpdateDelegate = callback;
        } else {
          this.mainCountUpdateDelegate = callback;
        }
    }

    @Override
    public void updateViewCount(Bee bee) {
        if (bee.getColorRGBA().toUpperCase().equals(BeeColors.RED.name())) {
            this.enemyCountUpdateDelegate.updateViewCount(this.enemyCharacterCount);
        } else {
            this.mainCountUpdateDelegate.updateViewCount(this.mainCharacterCount);
        }
    }
}

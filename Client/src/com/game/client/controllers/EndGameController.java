package com.game.client.controllers;

import com.game.client.logic.GameFileLogic;
import com.game.client.models.LeaderBoard;
import com.game.client.models.PlayerData;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EndGameController {

    @FXML
    private Label winMessage;
    @FXML
    private Label mainCount;
    @FXML
    private Label enemyCount;
    @FXML
    private Button againButton;
    @FXML
    private Button exitButton;
    @FXML
    private AnchorPane enterNameBox;
    @FXML
    private TextField textField;
    @FXML
    private Button confirmButton;

    private String playerName;

    private int playerScore;

    private LeaderBoard leaderBoard;

    private GameFileLogic<LeaderBoard> fileLogic;

    public EndGameController() {
        leaderBoard = new LeaderBoard();
        fileLogic = new GameFileLogic(LeaderBoard.class);
    }

    public void setUpMenu(boolean isMainWon, int mainScore, int enemyScore) {
        if(isMainWon) {
            textField.textProperty().addListener((observable, oldValue, newValue) -> {
                if(!newValue.isEmpty()) {
                    confirmButton.setDisable(false);
                } else {
                    confirmButton.setDisable(true);
                }
            });
            exitButton.setDisable(true);
            if(fileLogic.isLeaderBoardFileExist()) {
                leaderBoard = fileLogic.loadLeaderBoard();
            }
            winMessage.setId("message_win");

            enterNameBox.setVisible(true);
            playerScore = mainScore;
        } else {
            winMessage.setId("message_fail");
        }
        mainCount.setText("Your score: " + String.valueOf(mainScore));
        enemyCount.setText("Enemy score: " + String.valueOf(enemyScore));
    }

    public void handleAgainButton(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/GameArea.fxml"));
        AnchorPane personOverview = loader.load();
        GameAreaController controller = loader.getController();
        Stage newStage = (Stage)againButton.getScene().getWindow();
        newStage.setScene(new Scene(personOverview));
        newStage.show();
        controller.setUpControllerData(1, 30);
    }

    public void handleExitButton(ActionEvent event) throws IOException {
        Stage newStage = (Stage)exitButton.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/GameRegimeMenu.fxml"));
        Parent root = loader.load();
        GameRegimeController controller = loader.getController();
        newStage.setScene(new Scene(root));
        newStage.show();
        controller.checkLoadingAvailability();
    }

    public void handleConfirmButton(ActionEvent event) throws IOException {
            playerName = textField.getText();
            enterNameBox.setVisible(false);
            leaderBoard.addNewPlayer(playerName, playerScore);
            if (leaderBoard.getLeaderBoardData().size() > 5) {
                List<PlayerData> dataList = leaderBoard.getLeaderBoardData();
                Collections.sort(dataList, (o1, o2) -> o2.getScore().compareTo(o1.getScore()));
                dataList.remove(dataList.size() - 1);
                leaderBoard.setLeaderBoardData(dataList);
            }
            fileLogic.saveLeaderBoard(leaderBoard);
            exitButton.setDisable(false);
    }

}

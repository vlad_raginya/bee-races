package com.game.client.logic;

import com.game.client.datalayer.IRepository;
import com.game.client.datalayer.impl.Repository;
import com.game.client.models.GameSave;
import com.game.client.models.LeaderBoard;

import java.io.IOException;

public class GameFileLogic<T> {

    private IRepository<T> repository;
    public GameFileLogic(Class type) {
        repository = new Repository<T>(type);
    }
    public void saveGame(T savedData) throws IOException { repository.saveFile(savedData,"gamedata/lastSave.json"); }
    public void saveLeaderBoard(T leaderData) throws IOException { repository.saveFile(leaderData,"gamedata/leaderBoard.json"); }
    public T loadGame() { return (T)repository.loadFile("gamedata/lastSave.json"); }
    public T loadLeaderBoard() { return (T)repository.loadFile("gamedata/leaderBoard.json"); }
    public boolean isSaveFileExist() {
        return repository.isFileExist("gamedata/lastSave.json");
    }
    public boolean isLeaderBoardFileExist() { return repository.isFileExist("gamedata/leaderBoard.json"); }
}

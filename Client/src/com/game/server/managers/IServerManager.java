package com.game.server.managers;

import com.game.client.logic.delegates.GameAreaViewerDelegate;
import com.game.client.logic.delegates.GameUpdateDelegate;
import com.game.client.models.multiplayer.ConnectionResult;
import com.game.client.models.multiplayer.ServerState;

public interface IServerManager {
    ConnectionResult connectToServer(String ip, int port);

    ServerState hostGame(String ip, int port);

    void joinToServer(boolean join);

    boolean isUserJoined();

    void setGameAreaViewerCallback(GameAreaViewerDelegate gameAreaViewerCallback);

    void setGameUpdateCallback(GameUpdateDelegate gameUpdateCallback);

    void startServer(boolean start);
}

package com.game.client.logic.gameobject.impl;

import com.game.client.enums.BeeMoveDirections;
import com.game.client.logic.gameobject.IGameObjectLogic;
import com.game.client.models.KeyState;
import com.game.client.models.ObjectViewSet;
import com.game.client.models.Point;
import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.Flower;
import com.game.client.models.gameobjects.HitBox;
import javafx.geometry.Bounds;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;

import java.awt.event.KeyEvent;
import java.util.*;

public class GameObjectLogic implements IGameObjectLogic {
    @Override
    public HitBox createHitBox(Bounds bounds) {
        if (bounds == null) {
            return new HitBox(new Point(0, 0), new Point(0, 0));
        }

        double upperLeftX = bounds.getMinX();
        double upperLeftY = bounds.getMinY();

        double width = bounds.getWidth();
        double height = bounds.getHeight();

        double bottomRightX = upperLeftX + width;
        double bottomRightY = upperLeftY + height;

        return new HitBox(new Point(upperLeftX, upperLeftY), new Point(bottomRightX, bottomRightY));
    }

    @Override
    public HitBox createHitBox(double x, double y, double distance) {
        double upperLeftX = x;
        double upperLeftY = y;

        double bottomRightX = x + distance;
        double bottomRightY = y + distance;

        return new HitBox(new Point(upperLeftX, upperLeftY), new Point(bottomRightX, bottomRightY));
    }

    @Override
    public Bee moveBee(KeyCode keyCode, Bee bee, double maxWidth, double maxHeight, KeyState keyState) throws NullPointerException{
        if (bee == null) {
            throw new NullPointerException();
        }

        HitBox beeBox = bee.getHitBox();

        if (keyCode == KeyCode.D) {
            if (bee.getMoveDirection() != BeeMoveDirections.RIGHT) {
                bee.setMoveDirection(BeeMoveDirections.RIGHT);
                bee.setDirectionSkinEdit(false);
            }

            if (bee.getHitBox().getSecondPoint().getX() >= maxWidth) {
                return bee;
            }

            if (keyState.activeKeys.contains(KeyCode.W)) {
                if (bee.getHitBox().getFirstPoint().getY() <= 0) {
                    return bee;
                }

                beeBox = beeBox.moveUp(-bee.getSpeed());
            } else if (keyState.activeKeys.contains(KeyCode.S)) {
                if (bee.getHitBox().getSecondPoint().getY() >= maxHeight) {
                    return bee;
                }

                beeBox = beeBox.moveDown(-bee.getSpeed());
            }

            beeBox = beeBox.moveToRight(bee.getSpeed());
            return bee.updateHitBox(beeBox);
        } else if (keyCode == KeyCode.A) {
            if (bee.getMoveDirection() != BeeMoveDirections.LEFT) {
                bee.setMoveDirection(BeeMoveDirections.LEFT);
                bee.setDirectionSkinEdit(false);
            }

            if (bee.getHitBox().getFirstPoint().getX() <= 0) {
                return bee;
            }

            if (keyState.activeKeys.contains(KeyCode.W)) {
                if (bee.getHitBox().getFirstPoint().getY() <= 0) {
                    return bee;
                }

                beeBox = beeBox.moveUp(-bee.getSpeed());
            } else if (keyState.activeKeys.contains(KeyCode.S)) {
                if (bee.getHitBox().getSecondPoint().getY() >= maxHeight) {
                    return bee;
                }

                beeBox = beeBox.moveDown(-bee.getSpeed());
            }

            beeBox = beeBox.moveToLeft(bee.getSpeed());
            return bee.updateHitBox(beeBox);
        } else if (keyCode == KeyCode.W) {
            if (bee.getMoveDirection() != BeeMoveDirections.UP) {
                bee.setMoveDirection(BeeMoveDirections.UP);
                bee.setDirectionSkinEdit(false);
            }

            if (bee.getHitBox().getFirstPoint().getY() <= 0) {
                return bee;
            }

            if (keyState.activeKeys.contains(KeyCode.A)) {
                if (bee.getHitBox().getFirstPoint().getX() <= 0) {
                    return bee;
                }

                beeBox = beeBox.moveToLeft(bee.getSpeed());
            } else if (keyState.activeKeys.contains(KeyCode.D)) {
                if (bee.getHitBox().getSecondPoint().getX() >= maxWidth) {
                    return bee;
                }

                beeBox = beeBox.moveToRight(bee.getSpeed());
            }

            beeBox = beeBox.moveUp(-bee.getSpeed());  // В душе не знаю, почему так, но не трогайте
            return bee.updateHitBox(beeBox);
        } else if (keyCode == KeyCode.S) {
            if (bee.getMoveDirection() != BeeMoveDirections.DOWN) {
                bee.setMoveDirection(BeeMoveDirections.DOWN);
                bee.setDirectionSkinEdit(false);
            }

            if (bee.getHitBox().getSecondPoint().getY() >= maxHeight) {
                return bee;
            }

            if (keyState.activeKeys.contains(KeyCode.A)) {
                if (bee.getHitBox().getFirstPoint().getX() <= 0) {
                    return bee;
                }

                beeBox = beeBox.moveToLeft(bee.getSpeed());
            } else if (keyState.activeKeys.contains(KeyCode.D)) {
                if (bee.getHitBox().getSecondPoint().getX() >= maxWidth) {
                    return bee;
                }

                beeBox = beeBox.moveToRight(bee.getSpeed());
            }

            beeBox = beeBox.moveDown(-bee.getSpeed()); // В душе не знаю, почему так, но не трогайте
            return bee.updateHitBox(beeBox);
        }

        return bee;
    }

    @Override
    public Bee moveBee(BeeMoveDirections moveDirection, Bee bee, double maxWidth, double maxHeight) {
        KeyCode keyCode = KeyCode.D;
        KeyState keyState = new KeyState();

        switch (moveDirection) {
            case RIGHT: {
                keyCode = KeyCode.D;
                break;
            }
            case RIGHTDOWN: {
                keyCode = KeyCode.D;
                keyState.activeKeys.add(KeyCode.S);
                break;
            }
            case RIGHTUP: {
                keyCode = KeyCode.D;
                keyState.activeKeys.add(KeyCode.W);
                break;
            }
            case LEFT: {
                keyCode = KeyCode.A;
                break;
            }
            case LEFTDOWN: {
                keyCode = KeyCode.A;
                keyState.activeKeys.add(KeyCode.S);
                break;
            }
            case LEFTUP: {
                keyCode = KeyCode.A;
                keyState.activeKeys.add(KeyCode.W);
                break;
            }
            case DOWN: {
                keyCode = KeyCode.S;
                break;
            }
            case UP: {
                keyCode = KeyCode.W;
                break;
            }
        }

        return this.moveBee(keyCode, bee, maxWidth, maxHeight, keyState);
    }

    @Override
    public boolean isHitBoxesCrossed(HitBox hitBoxA, HitBox hitBoxB) {
        if (hitBoxA.getFirstPoint().getX() >= hitBoxB.getSecondPoint().getX()
                || hitBoxB.getFirstPoint().getX() >= hitBoxA.getSecondPoint().getX()) {
            return false;
        }

        if (hitBoxA.getFirstPoint().getY() >= hitBoxB.getSecondPoint().getY()
                || hitBoxB.getFirstPoint().getY() >= hitBoxA.getSecondPoint().getY()) {
            return false;
        }

        return true;
    }

    @Override
    public boolean isValidKey(KeyCode keyCode) {
        List<KeyCode> validKeyCodes = new ArrayList<>();
        validKeyCodes.add(KeyCode.A);
        validKeyCodes.add(KeyCode.S);
        validKeyCodes.add(KeyCode.D);
        validKeyCodes.add(KeyCode.W);

        if (validKeyCodes.contains(keyCode)) {
            return true;
        }

        return false;
    }

    @Override
    public boolean isPointContainsFlower(Point randomPoint, Collection<ObjectViewSet<Button, Flower>> flowersView, double distance) {
        Iterator<ObjectViewSet<Button, Flower>> iter = flowersView.iterator();
        HitBox newHitBox = createHitBox(randomPoint.getX(), randomPoint.getY(), distance);

        while (iter.hasNext()) {
            if (isHitBoxesCrossed(iter.next().getGameObject().getHitBox(), newHitBox)) {
                return true;
            }
        }

        return false;
    }
}

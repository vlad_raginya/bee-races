package com.game.client.models.gameobjects;

import com.game.client.models.Point;

import java.io.Serializable;

public class HitBox implements Serializable {

    private Point firstPoint;
    private Point secondPoint;

    public HitBox() {

    }

    public HitBox(Point first, Point second) {
        this.firstPoint = first;
        this.secondPoint = second;
    }

    public HitBox moveToRight(double distance) {
        Point upperLeft = firstPoint;
        Point bottomRight = secondPoint;

        upperLeft.setX(upperLeft.getX() + distance);
        bottomRight.setX(bottomRight.getX() + distance);
        return this;
    }

    public HitBox moveToRight() {
        return moveToRight(1);
    }

    public HitBox moveToLeft(double distance) {
        Point upperLeft = firstPoint;
        Point bottomRight = secondPoint;

        upperLeft.setX(upperLeft.getX() - distance);
        bottomRight.setX(bottomRight.getX() - distance);
        return this;
    }

    public HitBox moveToLeft() {
        return moveToLeft(1);
    }

    public HitBox moveUp(double distance) {
        Point upperLeft = firstPoint;
        Point bottomRight = secondPoint;

        upperLeft.setY(upperLeft.getY() + distance);
        bottomRight.setY(bottomRight.getY() + distance);
        return this;
    }

    public HitBox moveUp() {
        return moveUp(1);
    }

    public HitBox moveDown(double distance) {
        Point upperLeft = firstPoint;
        Point bottomRight = secondPoint;

        upperLeft.setY(upperLeft.getY() - distance);
        bottomRight.setY(bottomRight.getY() - distance);
        return this;
    }

    public HitBox moveDown() {
        return moveDown(1);
    }

    public Point getFirstPoint() {
        return this.firstPoint;
    }

    public Point getSecondPoint() {
        return this.secondPoint;
    }

    public void setFirstPoint(Point firstPoint) {
        this.firstPoint = firstPoint;
    }

    public void setSecondPoint(Point secondPoint) {
        this.secondPoint = secondPoint;
    }
}

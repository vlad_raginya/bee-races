package com.game.client.controllers;
import com.game.client.logic.GameFileLogic;
import com.game.client.models.LeaderBoard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {
    @FXML
    private Button start;
    @FXML
    private Button exit;
    @FXML
    private Button leaderBoard;

    private GameFileLogic<LeaderBoard> fileLogic;

    public Controller() {
        fileLogic = new GameFileLogic(LeaderBoard.class);
    }

    public void handleStartButton(ActionEvent event) throws Exception {
        Stage newStage = (Stage) start.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/GameRegimeMenu.fxml"));
        Parent root = loader.load();
        GameRegimeController controller = loader.getController();
        newStage.setScene(new Scene(root));
        newStage.show();
        controller.checkLoadingAvailability();
    }

    public void handleExitButton(ActionEvent event) {
        Stage stage = (Stage) exit.getScene().getWindow();
        stage.close();
    }

    public void handleLeaderBoardButton(ActionEvent event) throws IOException {
        LeaderBoard leaders = fileLogic.loadLeaderBoard();
        Stage newStage = (Stage) start.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/ScoreTable.fxml"));
        Parent root = loader.load();
        ScoreTableController controller = loader.getController();
        newStage.setScene(new Scene(root));
        newStage.show();
        controller.setUpLeaderBoardData(leaders);

    }

    public void checkLeaderBoardFile() {
        leaderBoard.setDisable(!fileLogic.isLeaderBoardFileExist());
    }
}

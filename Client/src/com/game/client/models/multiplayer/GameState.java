package com.game.client.models.multiplayer;

import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.Flower;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;

public class GameState implements Serializable {
    public Bee bee;
    private int timeMinutes;
    private int timeSeconds;
    private int beeScore;
    private HashMap<Integer, Flower> flowers;

    public GameState(Bee bee, int beeScore, HashMap<Integer, Flower> flowers) {
       this.bee = bee;
       this.beeScore = beeScore;
       this.flowers = flowers;
    }

    public Bee getBee() {
        return this.bee;
    }

    public int getBeeScore() {
        return this.beeScore;
    }

    public int getTimeMinutes() {
        return this.timeMinutes;
    }

    public int getTimeSeconds() {
        return this.timeSeconds;
    }

    public  HashMap<Integer, Flower> getFlowers() {
        return this.flowers;
    }

    public void setBee(Bee bee) {
        this.bee = bee;
    }

    public void setBeeScore(int beeScore) {
        this.beeScore = beeScore;
    }

    public void setFlowerList(HashMap<Integer, Flower> flower) {
        this.flowers = flowers;
    }

    public void setTimeMinutes(int timeMinutes) {
        this.timeMinutes = timeMinutes;
    }

    public void setTimeSeconds(int timeSeconds) {
        this.timeSeconds = timeSeconds;
    }
}

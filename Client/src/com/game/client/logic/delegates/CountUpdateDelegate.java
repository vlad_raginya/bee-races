package com.game.client.logic.delegates;

public interface CountUpdateDelegate {
    void updateViewCount(int count);
}

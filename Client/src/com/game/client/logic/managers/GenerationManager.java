package com.game.client.logic.managers;

import com.game.client.models.Point;

public interface GenerationManager<T> {
    Point generateRandomPoint(double maxX, double maxY);

    int getRandomValue(int lower, int upper);
}

package com.game.server.logic;

import com.game.client.models.GameSave;

import java.io.IOException;

public interface IServerLogic<T> {
    byte[] serialize(T data) throws IOException;

    T deserialize(byte[] data) throws IOException, ClassNotFoundException;
}

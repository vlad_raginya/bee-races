package com.game.shared.models;

public class ConnectPacket extends Packet {
    private String username;

    public ConnectPacket(byte[] data) {
        super(01);
        this.username = readData(data);
    }

    public String getUsername() {
        return this.username;
    }

    @Override
    public byte[] getData() {
        return ("01" + this.username).getBytes();
    }

    @Override
    public void writeData() {

    }
}

package com.game.client.logic.managers.impl;

import com.game.client.logic.delegates.BeePositionUpdateDelegate;
import com.game.client.logic.delegates.FlowerSpawnerDelegate;
import com.game.client.logic.gameobject.IGameObjectLogic;
import com.game.client.logic.gameobject.impl.GameObjectLogic;
import com.game.client.logic.managers.IEnemyManager;
import com.game.client.logic.managers.IGameCountManager;
import com.game.client.models.KeyState;
import com.game.client.models.ObjectViewSet;
import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.BeeHive;
import com.game.client.models.gameobjects.Flower;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

public class EnemyManager implements IEnemyManager  {
    private HashMap<Integer, ObjectViewSet<Button, Flower>> gameAreaFlowers;
    private ReentrantLock locker;
    private Bee currentBee;
    private boolean isGameCycleRunning = false;
    private IGameObjectLogic gameObjectLogic;
    private double maxWidth, maxHeight;
    private Button currentBeeView;
    private AnchorPane areaPane;
    private boolean isGameOnPause = false;
    private IGameCountManager gameCountManager;
    private ObjectViewSet<Button, BeeHive> enemyBeeHiveObjectView;
    private FlowerSpawnerDelegate flowerSpawnerDelegate;
    private BeePositionUpdateDelegate directionEditor;

    public EnemyManager(ReentrantLock locker,
                        Bee thisBee,
                        AnchorPane areaPane,
                        Button beeEnemy,
                        IGameCountManager gameCountManager,
                        ObjectViewSet<Button, BeeHive> enemyBeeHiveObjectView,
                        FlowerSpawnerDelegate flowerSpawnerDelegate,
                        BeePositionUpdateDelegate directionEditor) {
        this.gameAreaFlowers = new HashMap<Integer, ObjectViewSet<Button, Flower>>();
        this.locker = locker;
        this.currentBee = thisBee;
        gameObjectLogic = new GameObjectLogic();
        this.maxHeight = areaPane.getPrefHeight();
        this.maxWidth = areaPane.getPrefWidth();
        this.currentBeeView = beeEnemy;
        this.areaPane = areaPane;
        this.gameCountManager = gameCountManager;
        this.enemyBeeHiveObjectView = enemyBeeHiveObjectView;
        this.flowerSpawnerDelegate = flowerSpawnerDelegate;
        this.directionEditor = directionEditor;
    }

    @Override
    public void run() {
        isGameCycleRunning = true;
        while (isGameCycleRunning) {
            Map.Entry<Integer, ObjectViewSet<Button, Flower>> nearestFlower = getNearestFlower();
            if (nearestFlower == null) {
                return;
            }

            try {
                moveToFlower(nearestFlower, 0.1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

   @Override
    public void addGameAreaFlower(Integer key, ObjectViewSet<Button, Flower> flower) {
        locker.lock();
        this.gameAreaFlowers.put(key, flower);
        locker.unlock();
    }

    @Override
    public void removeGameAreaFlower(Integer key) {
        locker.lock();
        if (!this.gameAreaFlowers.isEmpty()) {
            this.gameAreaFlowers.remove(key);
        }
        locker.unlock();
    }

    @Override
    public void setGameAreaFlowers(HashMap<Integer, ObjectViewSet<Button, Flower>> flowersView) {
        locker.lock();
        this.gameAreaFlowers = flowersView;
        locker.unlock();
    }

    @Override
    public void disableEnemyBee(boolean disable) {
        isGameCycleRunning = !disable;
    }

    @Override
    public void pauseGame(boolean pause) {
            isGameOnPause = pause;
    }

    private Map.Entry<Integer, ObjectViewSet<Button, Flower>> getNearestFlower() {
        double shortestWay = 9999999;
        Map.Entry<Integer, ObjectViewSet<Button, Flower>> tempEntry = null;

        for (Map.Entry<Integer, ObjectViewSet<Button, Flower>> item : this.gameAreaFlowers.entrySet()) {
            double xDistance = item.getValue().getGameObject().getHitBox().getSecondPoint().getX() - this.currentBee.getHitBox().getFirstPoint().getX();
            double yDistance = item.getValue().getGameObject().getHitBox().getSecondPoint().getY() - this.currentBee.getHitBox().getFirstPoint().getY();

            double tempDistance = Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));

            if (tempDistance < shortestWay) {
                shortestWay = tempDistance;
                tempEntry = item;
            }
        }

        return tempEntry;
    }

    private void moveToFlower(Map.Entry<Integer, ObjectViewSet<Button, Flower>> nearestFlower, double enemySpeed) throws InterruptedException {
        KeyState keyState = new KeyState();
        while (!gameObjectLogic.isHitBoxesCrossed(this.currentBee.getHitBox(), nearestFlower.getValue().getGameObject().getHitBox())) {
            if (!isGameOnPause && isGameCycleRunning) {
                if (gameAreaFlowers.get(nearestFlower.getKey()).getGameObject().hashCode() != nearestFlower.getKey().hashCode()) {
                    nearestFlower = getNearestFlower();
                }
                Thread.sleep((int) (enemySpeed * 1000));
                if (this.currentBee.getHitBox().getFirstPoint().getX() > nearestFlower.getValue().getGameObject().getHitBox().getFirstPoint().getX()) {
                    gameObjectLogic.moveBee(KeyCode.A, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    if (this.currentBee.getHitBox().getFirstPoint().getY() > nearestFlower.getValue().getGameObject().getHitBox().getFirstPoint().getY()) {
                        gameObjectLogic.moveBee(KeyCode.W, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    }
                    if (this.currentBee.getHitBox().getFirstPoint().getY() < nearestFlower.getValue().getGameObject().getHitBox().getFirstPoint().getY()) {
                        gameObjectLogic.moveBee(KeyCode.S, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    }
                }
                if (this.currentBee.getHitBox().getFirstPoint().getX() < nearestFlower.getValue().getGameObject().getHitBox().getFirstPoint().getX()) {
                    gameObjectLogic.moveBee(KeyCode.D, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    if (this.currentBee.getHitBox().getFirstPoint().getY() > nearestFlower.getValue().getGameObject().getHitBox().getFirstPoint().getY()) {
                        gameObjectLogic.moveBee(KeyCode.W, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    }
                    if (this.currentBee.getHitBox().getFirstPoint().getY() < nearestFlower.getValue().getGameObject().getHitBox().getFirstPoint().getY()) {
                        gameObjectLogic.moveBee(KeyCode.S, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    }
                }

                this.currentBeeView.setLayoutX(this.currentBee.getHitBox().getFirstPoint().getX());
                this.currentBeeView.setLayoutY(this.currentBee.getHitBox().getFirstPoint().getY());

                if (!this.currentBee.isDirectionSkinEdit()) {
                    this.directionEditor.editBeeDirection(this.currentBee);
                    this.currentBee.setDirectionSkinEdit(true);
                }
            }
        }

        pickFlower(nearestFlower, enemySpeed);
    }

    private void pickFlower(Map.Entry<Integer, ObjectViewSet<Button, Flower>> nearestFlower, double speed) throws InterruptedException {
        this.removeGameAreaFlower(nearestFlower.getKey());
        Platform.runLater(() -> {
            areaPane.getChildren().remove(nearestFlower.getValue().getViewObject());
        });

        this.currentBee.setPickedFlower(nearestFlower.getValue().getGameObject());
        Platform.runLater(() -> {
            flowerSpawnerDelegate.spawnFlowerOnGameArea(1);
        });
        deliverFlower(speed);
    }

    private void deliverFlower(double enemySpeed) throws InterruptedException {
        KeyState keyState = new KeyState();
        while (!gameObjectLogic.isHitBoxesCrossed(this.currentBee.getHitBox(), enemyBeeHiveObjectView.getGameObject().getHitBox())) {
            if (!isGameOnPause && isGameCycleRunning) {
                Thread.sleep((int) (enemySpeed * 1000));
                if (this.currentBee.getHitBox().getFirstPoint().getX() > enemyBeeHiveObjectView.getGameObject().getHitBox().getFirstPoint().getX()) {
                    gameObjectLogic.moveBee(KeyCode.A, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    if (this.currentBee.getHitBox().getFirstPoint().getY() > enemyBeeHiveObjectView.getGameObject().getHitBox().getFirstPoint().getY()) {
                        gameObjectLogic.moveBee(KeyCode.W, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    }
                    if (this.currentBee.getHitBox().getFirstPoint().getY() < enemyBeeHiveObjectView.getGameObject().getHitBox().getFirstPoint().getY()) {
                        gameObjectLogic.moveBee(KeyCode.S, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    }
                }
                if (this.currentBee.getHitBox().getFirstPoint().getX() < enemyBeeHiveObjectView.getGameObject().getHitBox().getFirstPoint().getX()) {
                    gameObjectLogic.moveBee(KeyCode.D, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    if (this.currentBee.getHitBox().getFirstPoint().getY() > enemyBeeHiveObjectView.getGameObject().getHitBox().getFirstPoint().getY()) {
                        gameObjectLogic.moveBee(KeyCode.W, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    }
                    if (this.currentBee.getHitBox().getFirstPoint().getY() < enemyBeeHiveObjectView.getGameObject().getHitBox().getFirstPoint().getY()) {
                        gameObjectLogic.moveBee(KeyCode.S, this.currentBee, this.maxWidth, this.maxHeight, keyState);
                    }
                }

                this.currentBeeView.setLayoutX(this.currentBee.getHitBox().getFirstPoint().getX());
                this.currentBeeView.setLayoutY(this.currentBee.getHitBox().getFirstPoint().getY());
            }
        }

        this.gameCountManager.incrementCount(this.currentBee.getPickedFlower(), this.currentBee);
        this.currentBee.setHasFlower(false);
        Platform.runLater(() -> {
            this.gameCountManager.updateViewCount(this.currentBee);
        });
    }
}

package com.game.client.controllers;

import com.game.client.models.LeaderBoard;
import com.game.client.models.PlayerData;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScoreTableController {

    @FXML
    Button back;
    @FXML
    AnchorPane areaPane;

    public ScoreTableController() {}

    public void setUpLeaderBoardData(LeaderBoard leaders) {
        List<PlayerData> leadersData = leaders.getLeaderBoardData();
        Collections.sort(leadersData, (o1, o2) -> o2.getScore().compareTo(o1.getScore()));
        int shiftY = 200;
        for (PlayerData item :
                leadersData) {
            Label player = new Label();
            Label score = new Label();
            player.setText(item.getName());
            score.setText(item.getScore().toString());
            player.setLayoutX(200);
            player.setLayoutY(shiftY);
            score.setLayoutX(800);
            score.setLayoutY(shiftY);
            player.setId("text");
            score.setId("text");
            this.areaPane.getChildren().add(player);
            this.areaPane.getChildren().add(score);
            shiftY += 70;
        }
    }

    public void handleBackButton(ActionEvent event) throws IOException {
        Stage newStage = (Stage) back.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("../ui/Menu.fxml"));
        newStage.setScene(new Scene(root));
        newStage.show();
    }
}

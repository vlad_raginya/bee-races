package com.game.client.logic.managers.impl;

import com.game.client.enums.BeeMoveDirections;
import com.game.client.logic.delegates.BeePositionUpdateDelegate;
import com.game.client.logic.gameobject.IGameObjectLogic;
import com.game.client.logic.gameobject.impl.GameObjectLogic;
import com.game.client.logic.managers.IBeeMovementManager;
import com.game.client.models.KeyState;
import com.game.client.models.gameobjects.Bee;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

public class BeeMovementManager implements IBeeMovementManager {
    private TimerTask timerTask;
    private Timer timer;
    private ReentrantLock locker;
    private BeePositionUpdateDelegate beePositionUpdateFunction;
    private BeePositionUpdateDelegate directionEditFunction;
    private int updateFrequency;
    private Bee bee;
    private IGameObjectLogic gameObjectLogic;
    private BeeMoveDirections moveDirection;
    private double maxWidth;
    private double maxHeight;

    private boolean isGameOver = false;
    private boolean isBeeMoving = false;

    public BeeMovementManager(int updateFrequency,
                              BeePositionUpdateDelegate beePositionUpdateFunction,
                              BeePositionUpdateDelegate directionEditFunction,
                              Bee bee,
                              double maxWidth,
                              double maxHeight) {
        this.locker = new ReentrantLock();
        this.updateFrequency = updateFrequency;
        this.beePositionUpdateFunction = beePositionUpdateFunction;
        this.bee = bee;
        this.gameObjectLogic = new GameObjectLogic();
        timerTask = new BeeMovementManagerTask();
        timer = new Timer(true);
        this.maxHeight = maxHeight;
        this.maxWidth = maxWidth;
        this.directionEditFunction = directionEditFunction;
    }

    public void run() {
        timer.scheduleAtFixedRate(timerTask, 0, updateFrequency);
        while (!isGameOver) {
            continue;
        }

        timer.cancel();
    }

    @Override
    public void pauseGame(boolean pause) {
        if (pause) {
            timer.cancel();
        } else {
            timer = new Timer(true);
            timerTask = new BeeMovementManagerTask();
            timer.scheduleAtFixedRate(timerTask, 0, 500);
        }
    }

    @Override
    public void addSpeed(KeyCode keyCode, KeyState keyState) {
        if (keyCode == KeyCode.D) {
            if (keyState.activeKeys.contains(KeyCode.W)) {
                this.moveDirection = BeeMoveDirections.RIGHTUP;
            } else if (keyState.activeKeys.contains(KeyCode.S)) {
                this.moveDirection = BeeMoveDirections.RIGHTDOWN;
            } else {
                this.moveDirection = BeeMoveDirections.RIGHT;
            }

            isBeeMoving = true;
        } else if (keyCode == KeyCode.A) {
            if (keyState.activeKeys.contains(KeyCode.W)) {
                this.moveDirection = BeeMoveDirections.LEFTUP;
            } else if (keyState.activeKeys.contains(KeyCode.S)) {
                this.moveDirection = BeeMoveDirections.LEFTDOWN;
            } else {
                this.moveDirection = BeeMoveDirections.LEFT;
            }

            isBeeMoving = true;
        } else if (keyCode == KeyCode.W) {
            if (keyState.activeKeys.contains(KeyCode.A)) {
                this.moveDirection = BeeMoveDirections.LEFTUP;
            } else if (keyState.activeKeys.contains(KeyCode.D)) {
                this.moveDirection = BeeMoveDirections.RIGHTUP;
            } else {
                this.moveDirection = BeeMoveDirections.UP;
            }

            isBeeMoving = true;
        } else if (keyCode == KeyCode.S) {
            if (keyState.activeKeys.contains(KeyCode.A)) {
                this.moveDirection = BeeMoveDirections.LEFTDOWN;
            } else if (keyState.activeKeys.contains(KeyCode.D)) {
                this.moveDirection = BeeMoveDirections.RIGHTDOWN;
            } else {
                this.moveDirection = BeeMoveDirections.DOWN;
            }

            isBeeMoving = true;
        }
    }

    @Override
    public void takeOffSpeed(KeyState keyState) {
        if (keyState.activeKeys.size() != 0) {
            addSpeed(keyState.activeKeys.get(0), keyState);
        } else {
            isBeeMoving = false;
        }
    }

    private class BeeMovementManagerTask extends TimerTask {
        @Override
        public void run() {
            if (!isGameOver && isBeeMoving) {
                locker.lock();
                bee = gameObjectLogic.moveBee(moveDirection, bee, maxWidth, maxHeight);

                if (!bee.isDirectionSkinEdit()) {
                    directionEditFunction.editBeeDirection(bee);
                }

                Platform.runLater(() -> {
                    beePositionUpdateFunction.editBeeDirection(bee);
                });
                locker.unlock();
            }
        }
    }
}

package com.game.client.logic.managers;

import com.game.client.logic.Pauseable;

public interface ITimerManager extends Runnable, Pauseable {
    void setGameOver();
    int getMinutes();
    int getSeconds();
}

package com.game.client.logic.delegates;

import com.game.client.models.gameobjects.Bee;

public interface BeePositionUpdateDelegate {
    void editBeeDirection(Bee bee);
}

package com.game.client.models.factory;

import com.game.client.enums.BeeColors;
import com.game.client.models.Point;
import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.HitBox;
import com.game.client.models.gameobjects.base.GameObject;

public class BeeFactory implements GameObjectCreator<Bee> {
    @Override
    public Bee createObject(double firstX, double firstY, double secondX, double secondY, String color) {
        HitBox beeBox = new HitBox(new Point(firstX, firstY), new Point(secondX, secondY));

        if (beeBox != null) {
            return createBee(beeBox, color);
        }

        return new Bee(BeeColors.YELLOW.toString(), new HitBox(new Point(0.0, 0.0), new Point(0.0, 0.0)));
    }

    @Override
    public Bee createObject(HitBox hitBox, String color) {
        if (hitBox != null) {
            return createBee(hitBox, color);
        }

        return new Bee(BeeColors.YELLOW.toString(), new HitBox(new Point(0.0, 0.0), new Point(0.0, 0.0)));
    }

    private Bee createBee(HitBox beeBox, String color) {
        BeeColors beeColor = BeeColors.valueOf(color.toUpperCase());
        if (beeColor != null) {
            if (beeColor.name().toUpperCase().equals("YELLOW")
                    || beeColor.name().toUpperCase().equals("RED")) {
                return new Bee(color, beeBox);
            }
        }

        return new Bee(BeeColors.YELLOW.toString(), beeBox);
    }
}

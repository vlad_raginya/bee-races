package com.game.client.models.gameobjects.base;

import com.game.client.models.gameobjects.HitBox;

import java.io.Serializable;

public class GameObject implements Serializable {
    protected String colorRGBA;
    protected HitBox hitBox;

    public GameObject(String color, HitBox hitbox) {
        colorRGBA = color;
        hitBox = hitbox;
    }

    public GameObject() {
    }

    public String getColorRGBA() {
        return colorRGBA;
    }

    public HitBox getHitBox() {
        return hitBox;
    }
}

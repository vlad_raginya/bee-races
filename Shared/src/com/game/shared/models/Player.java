package com.game.shared.models;

import java.net.InetAddress;

public class Player {
    private int port;

    private InetAddress ipAddress;

    private String id;

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public Player(int port, InetAddress ipAddress) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.id = randomAlphaNumeric(18);
    }

    public String getId() {
        return this.id;
    }

    public int getPort() {
        return this.port;
    }

    public InetAddress getIpAddress() {
        return this.ipAddress;
    }

    private String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this.id == ((Player)o).getId()) {
            return true;
        }

        return true;
    }
}
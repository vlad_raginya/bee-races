package com.game.client.logic.managers;

import com.game.client.logic.Pauseable;
import com.game.client.models.ObjectViewSet;
import com.game.client.models.gameobjects.Flower;
import javafx.scene.control.Button;

import java.util.HashMap;

public interface IEnemyManager extends Runnable, Pauseable {
    void addGameAreaFlower(Integer key, ObjectViewSet<Button, Flower> flower);

    void removeGameAreaFlower(Integer key);

    void setGameAreaFlowers(HashMap<Integer, ObjectViewSet<Button, Flower>> flowersView);

    void disableEnemyBee(boolean disable);
}

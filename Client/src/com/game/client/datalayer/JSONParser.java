package com.game.client.datalayer;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.Objects;

public class JSONParser{

    public Object getObject(File file, Class c) {
        ObjectMapper mapper = new ObjectMapper();
        Object value = null;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            String ls = System.getProperty("line.separator");
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            reader.close();
            String jsonStr = stringBuilder.toString();
            value = mapper.readValue(jsonStr, c);

        }

        catch (IOException e) {
            e.printStackTrace();
        }

        return value;
    }

    public void saveObject(File file, Object o) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        try {

            String jsonStr = mapper.writeValueAsString(o);

            FileWriter writer = new FileWriter(file.getPath());
            writer.write(jsonStr);
            writer.close();
        }

        catch (IOException e) {
            e.printStackTrace();
        }

    }
}

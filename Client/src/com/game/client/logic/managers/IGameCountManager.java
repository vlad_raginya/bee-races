package com.game.client.logic.managers;

import com.game.client.logic.delegates.CountUpdateDelegate;
import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.Flower;

public interface IGameCountManager {
    int incrementCount(Flower flower, Bee beeToIncrement);

    int getEnemyCount();

    int getMainCount();

    void setEnemyCount(int count);

    void setMainCount(int count);

    void setCountUpdateCallback(Bee bee, CountUpdateDelegate callback);

    void updateViewCount(Bee bee);
}

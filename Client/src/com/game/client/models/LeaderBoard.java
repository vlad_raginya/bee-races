package com.game.client.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LeaderBoard {
    private List<PlayerData> leaderBoardData;

    public LeaderBoard() {
        leaderBoardData = new ArrayList<>();
    }

    public List<PlayerData> getLeaderBoardData() {
        return leaderBoardData;
    }

    public void setLeaderBoardData(List<PlayerData> leaderBoardData) {
        this.leaderBoardData = leaderBoardData;
    }

    public void addNewPlayer(String player, int score) {
        PlayerData data = new PlayerData(player, score);
        leaderBoardData.add(data);
    }
}

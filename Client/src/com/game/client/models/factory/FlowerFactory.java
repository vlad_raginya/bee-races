package com.game.client.models.factory;

import com.game.client.enums.FlowersEnum;
import com.game.client.models.Point;
import com.game.client.models.gameobjects.Flower;
import com.game.client.models.gameobjects.HitBox;

public class FlowerFactory implements GameObjectCreator<Flower>{
    @Override
    public Flower createObject(double firstX, double firstY, double secondX, double secondY, String color) {
        return null;
    }

    @Override
    public Flower createObject(HitBox hitBox, String color) {
        if (hitBox != null) {
            return createFlower(hitBox, color);
        }

        return new Flower(FlowersEnum.BLUE.name(), new HitBox(new Point(0, 0), new Point(0, 0)), FlowersEnum.BLUE, 6000);
    }

    private Flower createFlower(HitBox flowerBox, String color) {
        FlowersEnum flowerColor = FlowersEnum.valueOf(color.toUpperCase());

        if (flowerColor != null) {
            return new Flower(flowerColor.name(), flowerBox, flowerColor, 600);
        }

        return new Flower(FlowersEnum.BLUE.name(), flowerBox, FlowersEnum.BLUE, 6000);
    }
}

package com.game.client.controllers;

import com.game.client.enums.BeeColors;
import com.game.client.enums.FlowersEnum;
import com.game.client.enums.HiveColors;
import com.game.client.logic.delegates.*;
import com.game.client.logic.gameobject.IGameObjectLogic;
import com.game.client.logic.gameobject.impl.GameObjectLogic;
import com.game.client.logic.managers.*;
import com.game.client.logic.managers.impl.*;
import com.game.client.models.KeyState;
import com.game.client.models.ObjectViewSet;
import com.game.client.models.Point;
import com.game.client.models.factory.BeeFactory;
import com.game.client.models.factory.BeeHiveFactory;
import com.game.client.models.factory.FlowerFactory;
import com.game.client.models.factory.GameObjectCreator;
import com.game.client.models.gameobjects.Bee;
import com.game.client.models.gameobjects.BeeHive;
import com.game.client.models.gameobjects.Flower;
import com.game.client.models.gameobjects.HitBox;
import com.game.client.models.multiplayer.GameState;
import com.game.server.managers.IServerManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Map;

public class MultiplayerGameController {
    private HashMap<Integer, ObjectViewSet<Button, Flower>> flowersView;
    private GameObjectCreator<Bee> beeFactory;
    private GameObjectCreator<Flower> flowerFactory;
    private GameObjectCreator<BeeHive> beeHiveFactory;
    private IGameObjectLogic gameObjectLogic;
    private GenerationManager flowersManager;
    private IGameCountManager countManager;
    private IServerManager serverManager;
    private IBeeMovementManager beeMovementManager;
    private KeyState currentKeyState;

    private Button currentBeeView;
    private Button enemyBeeView;

    private Button currentHiveView;
    private Button enemyHiveView;

    private Bee currentBee;
    private Bee enemyBee;

    private BeeHive currentHive;
    private BeeHive enemyHive;

    private Label currentCountLabel;
    private Label enemyCountLabel;



    private boolean isGameHost;
    private static int MAX_FLOWERS_COUNT = 5;
    private static int MAX_CHILDREN_COUNT = 12;

    @FXML
    private Button houseBlackView;
    @FXML
    private Button houseYellowView;
    @FXML
    private Button beeRedView;
    @FXML
    private Button beeYellowView;
    @FXML
    private AnchorPane areaPane;
    @FXML
    private AnchorPane popUp;
    @FXML
    private Label yellowBeeCountView;
    @FXML
    private Label redBeeCountView;
    @FXML
    private Label timerLabel;

    public MultiplayerGameController() {
        beeFactory = new BeeFactory();
        beeHiveFactory = new BeeHiveFactory();
        gameObjectLogic = new GameObjectLogic();
        flowersManager = new FlowersSpawnerManager();
        flowerFactory = new FlowerFactory();
        countManager = new GameCountManager();
        currentKeyState = new KeyState();

        flowersView = new HashMap<>();
    }

    public void keyPressed(KeyEvent event) {
        if (!serverManager.isUserJoined()) {
            return;
        }

//        if (event.getCode() == KeyCode.ESCAPE) {
//            if (isGameOnPause) {
//                return;
//            }
//        }
//
//        if (isGameOnPause) {
//            return;
//        }

        if (!this.currentKeyState.activeKeys.contains(event.getCode()) && this.currentKeyState.activeKeys.size() < 2 && gameObjectLogic.isValidKey(event.getCode())) {
            this.currentKeyState.activeKeys.add(event.getCode());
        } else {
            return;
        }

        beeMovementManager.addSpeed(event.getCode(), this.currentKeyState);
        checkForFlowerDeliver();

        int indexToRemove = -1;

        for (Map.Entry<Integer, ObjectViewSet<Button, Flower>> item : flowersView.entrySet()) {
            if (gameObjectLogic.isHitBoxesCrossed(this.currentBee.getHitBox(),
                    item.getValue().getGameObject().getHitBox())) {
                indexToRemove = item.getKey();
            }
        }

        if (indexToRemove != -1) {
            if (this.currentBee.isHasFlower()) {
                return;
            }

            areaPane.getChildren().remove(indexToRemove);
            pickFlower(flowersView.get(indexToRemove).getGameObject(), indexToRemove);

            flowersView.remove(flowersView.get(indexToRemove));
        }
    }

    public void keyReleased(KeyEvent event) {
        if (this.currentKeyState.activeKeys.contains(event.getCode())) {
            this.currentKeyState.activeKeys.remove(event.getCode());
            beeMovementManager.takeOffSpeed(this.currentKeyState);
        }
    }

//    public void handleExitButton(ActionEvent event) throws IOException {
//        this.timerManager.setGameOver();
//        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/GameRegimeMenu.fxml"));
//        AnchorPane personOverview = loader.load();
//        GameRegimeController controller = loader.getController();
//        Stage newStage = (Stage)houseMain.getScene().getWindow();
//        newStage.setScene(new Scene(personOverview));
//        newStage.show();
//        controller.checkLoadingAvailability();
//    }

    public void setUpControllerData(boolean isGameHost, IServerManager serverManager) {
        this.isGameHost = isGameHost;
        this.serverManager = serverManager;

        if (isGameHost) {
            setUpCurrentViewGameObjects(this.houseYellowView, this.beeYellowView, this.houseBlackView, this.beeRedView, this.yellowBeeCountView, this.redBeeCountView);
            setUpCurrentGameObjects(BeeColors.YELLOW, HiveColors.YELLOW, BeeColors.RED, HiveColors.BLACK);
            spawnFlowers(MAX_FLOWERS_COUNT);
        } else {
            setUpCurrentViewGameObjects(this.houseBlackView, this.beeRedView, this.houseYellowView, this.beeYellowView, this.redBeeCountView, this.yellowBeeCountView);
            setUpCurrentGameObjects(BeeColors.RED, HiveColors.BLACK, BeeColors.YELLOW, HiveColors.YELLOW);
        }

        beeMovementManager = new BeeMovementManager(50, beePositionUpdate, directionEditor, this.currentBee, areaPane.getPrefWidth(), areaPane.getPrefHeight());
        serverManager.setGameAreaViewerCallback(this.gameAreaViewerCallback);
        serverManager.setGameUpdateCallback(this.gameUpdateCallback);

        Thread beeMovementManagerThread = new Thread(beeMovementManager);
        beeMovementManagerThread.start();

        if (isGameHost) {
            serverManager.startServer(true);
        } else {
            serverManager.joinToServer(true);
        }
    }

    private void setUpCurrentViewGameObjects(Button currentHive, Button currentBee, Button enemyHive, Button enemyBee, Label currentCount, Label enemyCount) {
        this.currentHiveView = currentHive;
        this.currentBeeView = currentBee;
        this.enemyHiveView = enemyHive;
        this.enemyBeeView = enemyBee;
        this.currentCountLabel = currentCount;
        this.enemyCountLabel = enemyCount;
    }

    private void setUpCurrentGameObjects(BeeColors currentBeeColor, HiveColors currentHiveColor, BeeColors enemyBeeColor, HiveColors enemyHiveColor) {
        createBees(enemyBeeColor, currentBeeColor);
        createBeeHives(enemyHiveColor, currentHiveColor);
    }

    private void createBeeHives(HiveColors enemyColor, HiveColors currentColor) {
        Bounds currentBeeHiveBounds = this.currentHiveView.localToScene(this.currentHiveView.getBoundsInLocal());
        HitBox currentBeeHiveHitBox = this.gameObjectLogic.createHitBox(currentBeeHiveBounds);

        Bounds enemyBeeHiveBounds = this.enemyHiveView.localToScene(this.enemyHiveView.getBoundsInLocal());
        HitBox enemyBeeHiveHitBox = this.gameObjectLogic.createHitBox(enemyBeeHiveBounds);

        this.currentHive = beeHiveFactory.createObject(currentBeeHiveHitBox, currentColor.name());
        this.enemyHive = beeHiveFactory.createObject(enemyBeeHiveHitBox, enemyColor.name());
    }

    private void createBees(BeeColors enemyColor, BeeColors currentColor) {
        Bounds currentBeeBounds = this.currentBeeView.localToScene(this.currentBeeView.getBoundsInLocal());
        HitBox currentBeeHitBox = this.gameObjectLogic.createHitBox(currentBeeBounds);

        Bounds enemyBeeBounds = this.enemyBeeView.localToScene(this.enemyBeeView.getBoundsInLocal());
        HitBox enemyBeeHitBox = this.gameObjectLogic.createHitBox(enemyBeeBounds);

        this.currentBee = beeFactory.createObject(currentBeeHitBox, currentColor.name());
        this.enemyBee = beeFactory.createObject(enemyBeeHitBox, enemyColor.name());
    }

    private void spawnFlowers(int flowerCount) {
        for (int i = 0; i < flowerCount; i++) {
            flowerSpawnFunction.spawnFlowerOnGameArea(flowerCount);
        }
    }

    FlowerSpawnerDelegate flowerSpawnFunction = (maxCountFlowers) -> {
        int flowerColor = flowersManager.getRandomValue(0, 3);

        Image image = getFlowerImage(FlowersEnum.values()[flowerColor]);

        Button flower = createViewFlower(image);

        Bounds flowerBounds = flower.localToScene(flower.getBoundsInLocal());
        HitBox flowerBox = gameObjectLogic.createHitBox(flowerBounds.getMaxX(), flowerBounds.getMaxY(), 40);

        Flower createdFlower = flowerFactory.createObject(flowerBox, FlowersEnum.values()[flowerColor].name());
        addFlowerToCollection(flower, createdFlower);
    };

    private void addFlowerToCollection(Button flowerView, Flower flower) {
        int indexToPut = MAX_CHILDREN_COUNT - MAX_FLOWERS_COUNT;
        int lastIndex = indexToPut + MAX_FLOWERS_COUNT;

        while(indexToPut < lastIndex) {
            if (this.flowersView.containsKey(indexToPut)) {
                indexToPut ++;
                continue;
            }

            break;
        }

        ObjectViewSet viewValue = new ObjectViewSet<Button, Flower>(flowerView, flower);

        this.flowersView.put(indexToPut, viewValue);
        this.areaPane.getChildren().add(flowerView);
    }

    private Image getFlowerImage(FlowersEnum flowerColor) {
        Image image;

        switch (flowerColor) {
            case RED: {
                image = new Image(getClass().getResourceAsStream("../ui/styles/Images/flower_red.png"), 40, 40, false, false);
                break;
            }
            case YELLOW: {
                image = new Image(getClass().getResourceAsStream("../ui/styles/Images/flower_yellow.png"), 40, 40, false, false);
                break;
            }
            default: {
                image = new Image(getClass().getResourceAsStream("../ui/styles/Images/flower_blue.png"), 40, 40, false, false);
                break;
            }
        }

        return image;
    }

    private Button createViewFlower(Image image) {
        ImageView imageView = new ImageView(image);
        Button flower = new Button("", imageView);

        Stage newStage = (Stage) enemyBeeView.getScene().getWindow();
        Point point = flowersManager.generateRandomPoint(newStage.getWidth(), newStage.getHeight());

        while (gameObjectLogic.isPointContainsFlower(point, flowersView.values(), 40)) {
            point = flowersManager.generateRandomPoint(newStage.getWidth(), newStage.getHeight());
        }

        setFlowerStyles(flower, point);

        return flower;
    }

    private void setFlowerStyles(Button flower, Point point) {
        flower.setLayoutX(point.getX());
        flower.setLayoutY(point.getY());
        flower.setMaxHeight(40);
        flower.setMaxWidth(40);

        flower.setStyle("-fx-base: none;");
    }

    private void pickFlower(Flower flower, Integer key) {
        this.currentBee.setPickedFlower(flower);
        this.currentBee.setHasFlower(true);
        spawnFlowers(1);
    }

    private void checkForFlowerDeliver() {
        if (gameObjectLogic.isHitBoxesCrossed(this.currentHive.getHitBox(), this.currentBee.getHitBox())
                && this.currentBee.isHasFlower()) {
            this.currentBee.setHasFlower(false);
            this.currentCountLabel.setText(Integer.toString(Integer.parseInt(this.currentCountLabel.getText()) + 1));
        }
    }

    GameUpdateDelegate gameUpdateCallback = (gameState, locker) -> {
        Platform.runLater(() -> {
            this.enemyCountLabel.setText(Integer.toString(gameState.getBeeScore()));
            updateGameTimer(gameState.getTimeMinutes(), gameState.getTimeSeconds());
            updateFlowers(gameState.getFlowers());
            updateEnemyBee(gameState.bee);
            if (locker.isLocked()) {
                locker.unlock();
            }
        });
    };

    GameAreaViewerDelegate gameAreaViewerCallback = () -> {
        GameState currentState = new GameState(this.currentBee, Integer.parseInt(this.currentCountLabel.getText()), initializeFlowers());

        return currentState;
    };

    BeePositionUpdateDelegate beePositionUpdate = (bee) -> {
        this.currentBeeView.setLayoutX(bee.getHitBox().getFirstPoint().getX());
        this.currentBeeView.setLayoutY(bee.getHitBox().getFirstPoint().getY());
    };

//    GameOverDelegate gameOverFunction = () -> {
//        int mainScore = countManager.getMainCount();
//        int enemyScore = countManager.getEnemyCount();
//        FXMLLoader loader = new FXMLLoader(getClass().getResource("../ui/EndGameMenu.fxml"));
//        AnchorPane personOverview = loader.load();
//        EndGameController controller = loader.getController();
//        Stage newStage = (Stage)houseMain.getScene().getWindow();
//        newStage.setScene(new Scene(personOverview));
//        newStage.show();
//        controller.setUpMenu(mainScore > enemyScore, mainScore, enemyScore);
//    };

    BeePositionUpdateDelegate directionEditor = (bee) -> {
        Background background = getBackgroundForBee(bee);

        this.currentBeeView.setBackground(background);
    };

    private Background getBackgroundForBee(Bee bee) {
        int beeSkin = 1;

        if (bee.getColorRGBA().toUpperCase().equals(BeeColors.RED.name())) {
            beeSkin = 2;
        }

        Image image = getBeeDirectionImage(bee, beeSkin);

        BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);

        return background;
    }

    private HashMap<Integer, Flower> initializeFlowers() {
        HashMap<Integer, Flower> flowers = new HashMap<>();

        for (Map.Entry<Integer, ObjectViewSet<Button, Flower>> item : flowersView.entrySet()) {
            flowers.put(item.getKey(), item.getValue().getGameObject());
        }

        return  flowers;
    }

    private void updateGameTimer(int minutes, int seconds) {
        if (seconds >= 10) {
            this.timerLabel.setText(String.format("%d:%d", minutes, seconds));
        } else {
            this.timerLabel.setText(String.format("%d:0%d", minutes, seconds));
        }
    }

    private void updateEnemyBee(Bee bee) {
        this.enemyBee = bee;
        Background background = getBackgroundForBee(bee);
        this.enemyBeeView.setBackground(background);

        this.enemyBeeView.setLayoutX(bee.getHitBox().getFirstPoint().getX());
        this.enemyBeeView.setLayoutY(bee.getHitBox().getFirstPoint().getY());
    }

    private void updateFlowers(HashMap<Integer, Flower> flowersView) {
            this.flowersView.clear();
//            for (int i = MAX_CHILDREN_COUNT - 1; i >= MAX_CHILDREN_COUNT - MAX_FLOWERS_COUNT; --i) {
//                this.areaPane.getChildren().remove(i);
//                this.areaPane.getChildren().add();
//            }

            //int i = MAX_CHILDREN_COUNT - 1;
            for (Map.Entry<Integer, Flower> item : flowersView.entrySet()) {
                if (this.areaPane.getChildren().size() > (int)item.getKey()) {
                    this.areaPane.getChildren().remove((int) item.getKey());
                }
                    Button newFlower = createFlower(item.getValue());
                    this.flowersView.put(item.getKey(), new ObjectViewSet<>(newFlower, item.getValue()));
                    this.areaPane.getChildren().add(item.getKey(), newFlower);
                    // update local flowerView

            }
    }

    private Button createFlower(Flower flower) {
        Image flowerImg = getFlowerImage(flower.getColor());
        ImageView imageView = new ImageView(flowerImg);
        Button flowerView = new Button("", imageView);

        setFlowerStyles(flowerView, flower.getHitBox().getFirstPoint());
        return flowerView;
    }

    private Image getBeeDirectionImage(Bee bee, int beeSkin) {
        Image image;

        switch (bee.getMoveDirection()) {
            case LEFT: {
                image = new Image(getClass().getResourceAsStream(String.format("../ui/styles/Images/bee_left_%d.png", beeSkin)), 50, 50, false, false);
                break;
            }
            case UP: {
                image = new Image(getClass().getResourceAsStream(String.format("../ui/styles/Images/bee_up_%d.png", beeSkin)), 50, 50, false, false);
                break;
            }
            case DOWN: {
                image = new Image(getClass().getResourceAsStream(String.format("../ui/styles/Images/bee_down_%d.png", beeSkin)), 50, 50, false, false);
                break;
            }
            default: {
                image = new Image(getClass().getResourceAsStream(String.format("../ui/styles/Images/bee_right_%d.png", beeSkin)), 50, 50, false, false);
                break;
            }
        }

        return image;
    }

    protected void finalize() {
        if (this.serverManager != null) {
            if (isGameHost) {
                this.serverManager.startServer(false);
            } else {
                this.serverManager.joinToServer(false);
            }
        }
    }
}

package com.game.client.logic;

public interface Pauseable {
    void pauseGame(boolean pause);
}

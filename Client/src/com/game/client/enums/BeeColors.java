package com.game.client.enums;

import java.io.Serializable;

public enum BeeColors implements Serializable {
    YELLOW,
    RED
}

package com.game.client.logic.managers.impl;

import com.game.client.logic.managers.GenerationManager;
import com.game.client.models.Point;
import com.game.client.models.gameobjects.Flower;

public class FlowersSpawnerManager implements GenerationManager<Flower> {
    @Override
    public Point generateRandomPoint(double maxX, double maxY) {
        double randomX = 400 + (Math.random()* (maxX-800));
        double randomY = 100 + (Math.random()* (maxY-200));

        return new Point(randomX, randomY);
    }

    @Override
    public int getRandomValue(int lower, int upper) {
        int blueProbability = 15;
        int redProbability = 35;
        int yellowProbability = 50;
        int random =  (int)(Math.random() * 100);

        if (random <= blueProbability) {
            return 2;
        } else if (random > redProbability && random <= yellowProbability) {
            return 1;
        } else {
            return 0;
        }
    }
}
